package;

import cv.Core;
import cv.ImgProc;
import cv.core.Mat;

// General image processor
interface ImageProcessor {
    public function process(min:Mat, out:Mat):Void;
}

// Composition of image processors
class MultiProcessor implements ImageProcessor {
    public var processors:Array<ImageProcessor>;
    public var tmp:Mat;
    public function new(?processors:Null<Array<ImageProcessor>>) {
        this.processors = if (processors == null) [] else processors;
    }
    public function process(min:Mat, out:Mat) {
        switch (processors.length) {
        case 0: Core.copy(min, out);
        case 1: processors[0].process(min, out);
        case n:
            if (tmp == null
             || tmp.cols != min.cols
             || tmp.rows != min.rows) tmp = Core.cloneMat(min);
            var tin = min;
            var tout = if (n%2==0) tmp else out;
            for (p in processors) {
                p.process(tin, tout);
                tin = tout;
                tout = if (tout == tmp) out else tmp;
            }
        }
    }
}

// Median blur processor
class MedianProcessor implements ImageProcessor {
    public var kSize:Int;
    public function new(kSize:Int) {
        this.kSize = kSize;
    }
    public function process(min:Mat, out:Mat) {
        ImgProc.medianBlur(min, out, kSize);
    }
}

// Gaussian blur processor
class GaussianProcessor implements ImageProcessor {
    public var kSize:Int;
    public var ox:Float;
    public var oy:Float;
    public function new(kSize:Int, ox:Float=0, oy:Float=0) {
        this.kSize = kSize;
        this.ox = ox;
        this.oy = oy;
    }
    public function process(min:Mat, out:Mat) {
        ImgProc.gaussianBlur(min, out, Core.size(kSize, kSize), ox, oy);
    }
}

// Bilateral filtering processor
class BilateralProcessor implements ImageProcessor {
    public var d:Int;
    public var sigmaColor:Float;
    public var sigmaSpace:Float;
    public function new(d:Int, sigmaColor:Float, sigmaSpace:Float) {
        this.d = d;
        this.sigmaColor = sigmaColor;
        this.sigmaSpace = sigmaSpace;
    }
    public function process(min:Mat, out:Mat) {
        ImgProc.bilateralFilter(min, out, d, sigmaColor, sigmaSpace);
    }
}

// Histogram Equalisation processor
class HistogramProcessor implements ImageProcessor {
    public function new() {
    }
    public function process(min:Mat, out:Mat) {
        ImgProc.equalizeHist(min, out);
    }
}

// Adaptive Histogram Equalisation processor
class AdaptiveHistogramProcessor implements ImageProcessor {
    public var windowRows:Int;
    public var windowCols:Int;
    public var limit:Float;
    public function new(windowRows:Int, windowCols:Int, limit:Float=0.05) {
        this.windowRows = windowRows;
        this.windowCols = windowCols;
        this.limit = limit;
    }
    public function process(min:Mat, out:Mat) {
        ImgProc.equalizeHistAdaptive(min, out, windowRows, windowCols, limit);
    }
}

// Custom processor
class CustomProcessor implements ImageProcessor {
    public var f:Mat->Mat->Void;
    public function new(f:Mat->Mat->Void) {
        this.f = f;
    }
    public function process(min:Mat, out:Mat) {
        f(min, out);
    }
}

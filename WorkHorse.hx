package;

import #if cpp cpp #else neko #end.vm.Thread;
import cv.core.Mat;
import cv.Core;
import glgui.GLFWEventLoop;
import glfw3.GLFW;
import goodies.Maybe;
import Main;
import ImageProcessor;
import FeatureDetector;
import Triangulate;
import TrackingPane;

enum WorkState {
    WSIdle;
    WSTracking;
    WSAnimate;
}

class WorkHorse {
    public static var self(default,null):Thread; // shared

    public var video:Video;
    public var cFrame:Mat;

    public var processor:ImageProcessor;
    public var pFrame:Mat;

    public var detector:FeatureDetector;
    public var features:Array<Feature>;
    public var featureCount:Int;

    public var tracking:TrackingInfo;

    public var state:WorkState;
    public var prevFrame:Mat;

    static inline var syncTime = 1/60;
    var frame:Null<Int>;
    public var history:Array<{
        fs:Array<Feature>,
        cnt:Int
    }>;
    public var ts:Array<Int>; // subdivided triangulation of feature points.
    public var tvs:Array<Array<PV>>; // subdivided triangulatino points + colours

    function eval(fs:Array<Feature>,ts:Array<Int>):Array<TValues> {
        var out = [];
        for (i in 0...Std.int(ts.length/3)) {
            var t = i*3;
            var t0 = ts[t+0];
            var t1 = ts[t+1];
            var t2 = ts[t+2];
            var f0 = fs[t0];
            var f1 = fs[t1];
            var f2 = fs[t2];
            var e0x = f1.x-f2.x; var e0y = f1.y-f2.y;
            var e1x = f2.x-f0.x; var e1y = f2.y-f0.y;
            var e2x = f0.x-f1.x; var e2y = f0.y-f1.y;
            var e0 = Math.sqrt(e0x*e0x+e0y*e0y);
            var e1 = Math.sqrt(e1x*e1x+e1y*e1y);
            var e2 = Math.sqrt(e2x*e2x+e2y*e2y);
            out.push({
                e0: e0, e1: e1, e2: e2
            });
        }
        return out;
    }

    function delta(a:Array<TValues>, init:Array<TValues>):Array<TValues> {
        var out = [];
        for (i in 0...a.length) {
            var a = a[i];
            var init = init[i];
            out.push({
                e0: (a.e0-init.e0)/init.e0,
                e1: (a.e1-init.e1)/init.e1,
                e2: (a.e2-init.e2)/init.e2
            });
        }
        return out;
    }

    public function compute() {
        if (history.length == 0) {
            Main.window.thread.sendMessage(TOther(SLog(' # Nothing to do')));
            return;
        }
        Main.window.thread.sendMessage(TOther(SLog(' # Start Analysis')));

        var ps = [for (h in history) [for (i in 0...h.cnt) {x:h.fs[i].x,y:h.fs[i].y}]];
        var ts = Triangulate.triangulate(ps[0]);

        var evals = [for (h in history) eval(h.fs,ts)];
        var strains = [for (i in 0...history.length) delta(evals[i],evals[0])];

        var subts;
        var res0 = Triangulate.divide(ps[0],ts,strains[0]);
        subts = this.ts = res0.ts;
        tvs = [res0.fs];
        for (i in 1...history.length) tvs.push(Triangulate.divide(ps[i],ts,strains[i],true).fs);

        var maxu = -1e10;
        var minu = 1e10;
        for (i in 0...history.length) {
            for (p in tvs[i]) {
                if (p.cnt == 0) p.u = 0;
                else {
                    p.u /= p.cnt;
                    if (p.u > maxu) maxu = p.u;
                    if (p.u < minu) minu = p.u;
                }
            }
        }
        if (maxu == 0) maxu = 1;
        for (i in 0...history.length) {
            for (p in tvs[i]) {
                p.u = (p.u - minu)/(maxu - minu);
                p.u = Math.log(1 + p.u*(Math.exp(1)-1));
            }
        }

        Main.window.thread.sendMessage(TOther(SLog(' # Finished')));
    }

    public function process() {
        if (cFrame == null) return;
        pFrame = Core.cloneMat(cFrame);
        if (processor == null)
             Core.copy(cFrame, pFrame);
        else processor.process(cFrame, pFrame);
    }

    public function detect() {
        if (pFrame == null) return;
        if (detector != null) {
            featureCount = detector.detect(pFrame, features);
            history = [{fs:features,cnt:featureCount}];
        }
    }

    public static function main() {
        new WorkHorse();
    }

    public function handleAnyState(msg:TMessage) {
        switch (msg) {
        case TTerminate: return true;
        case TOther(ssg): var msg:SMessage = ssg;
        switch (msg) {
            case SQueryFrame(ret, p):
                GLFWEventLoop.track('@ \033[36mWorkHorse\033[m Frame -> [thread]');
                ret.sendMessage(TOther(SFrame(p ? pFrame : cFrame)));
            case SQueryFeatures(ret):
                GLFWEventLoop.track('@ \033[36mWorkHorse\033[m Features -> [thread]');
                ret.sendMessage(TOther(SFeatures(features, featureCount)));
            case SQueryMap(ret):
                GLFWEventLoop.track('@ \033[36mWorkHorse\033[m Map -> [thread]');
                if (tvs == null || frame == null) ret.sendMessage(TOther(SMap(null,null)));
                else ret.sendMessage(TOther(SMap(tvs[frame],ts)));
            case SReset:
                state = WSIdle;
                prevFrame = null;
                history = null;
                ts = null;
                tvs = null;
                frame = null;
                video.restart();
                cFrame = video.update().extract();
                pFrame = Core.cloneMat(cFrame);
                process();
                detect();
                Main.window.thread.sendMessage(TOther(SLog(
                    ' # Reset'
                )));
            default:
            }
        default:
        }
        return false;
    }

    public function handleIdleState(msg:TMessage) {
        switch (msg) {
        case TOther(ssg): var msg:SMessage = ssg;
        switch (msg) {
            case SSetVideo(vid):
                video = vid;
                cFrame = video.update().extract();
                process();
                detect();
            case SSetProcessor(p):
                processor = p;
                process();
                detect();
            case SSetDetector(d):
                detector = d;
                detect();
            case SSetTracking(info):
                tracking = info;
            case SAnimate:
                state = WSAnimate;
                frame = 0;
                sync = GLFW.getTime() + syncTime;
            case SStartTrack:
                state = WSTracking;
                history = [];
                Main.window.thread.sendMessage(TOther(SLog(
                    ' # Start Tracking'
                )));
            case SDump:
                var path = systools.Dialogs.saveFile("Select dump location", "", Sys.getCwd());
                if (path != null) {
                    var out = sys.io.File.write(""+path, true);
                    out.writeInt32(history.length);
                    if (history.length > 0)
                        out.writeInt32(history[0].cnt);
                    for (h in history) {
                        for (i in 0...h.cnt) {
                            out.writeFloat(h.fs[i].x);
                            out.writeFloat(h.fs[i].y);
                        }
                    }
                    out.flush();
                    out.close();
                    var out = sys.io.File.write(""+path+".other", false);
                    out.writeString('${history.length}\n');
                    if (history.length > 0)
                        out.writeString('${history[0].cnt}\n');
                    for (h in history) {
                        for (i in 0...h.cnt) {
                            out.writeString('${h.fs[i].x}\n');
                            out.writeString('${h.fs[i].y}\n');
                        }
                    }
                    out.flush();
                    out.close();
                }
            case SCompute:
                compute();
            default:
            }
        default:
        }
    }

    public var sync:Float;
    public function syncFrame():Bool {
        if (GLFW.getTime() > sync) {
            sync = GLFW.getTime() + syncTime;
            cFrame = video.frame(frame).extract();
            process();
            features = history[frame].fs;
            featureCount = history[frame].cnt;
            frame++;
        }
        return frame == history.length;
    }

    public function track():Bool {
        var next = video.update();
        if (next == null) return true;

        prevFrame = Core.cloneMat(pFrame);
        cFrame = next.extract();
        process();

        var pPositions = [];
        var cPositions = [];
        for (i in 0...featureCount) {
            var f = features[i];
            pPositions.push(cv.Core.point2D32f(f.x, f.y));
            if (f.vx != null)
                cPositions.push(cv.Core.point2D32f(f.x+f.vx, f.y+f.vy));
            else
                cPositions.push(cv.Core.point2D32f(f.x,f.y));
        }
        var status = [];
        var error = [];
        cv.Video.calcOpticalFlowPyrLK(
            prevFrame,
            pFrame,
            null,
            null,
            pPositions,
            cPositions,
            Core.size(tracking.window, tracking.window),
            tracking.pyramids,
            status,
            error,
            Core.termCriteria(Core.TERMCRIT_ITER, tracking.iterations, 0),
            cv.Video.LKFLOW_INITIAL_GUESSES
        );
        var nFeatures = [];

        var smooth = tracking.smooth;
        var errored = [];
        for (i in 0...status.length) {
            if (!status[i] || error[i] > tracking.maxError) {
                var f = features[i].weakCopy();
                errored.push({
                    status : status[i],
                    feature: f,
                    opos   : [features[i].x,features[i].y],
                    found  : cPositions[i],
                    ind: nFeatures.length
                });
                nFeatures.push(f);
            }
            else if (status[i]) {
                var f = features[i].weakCopy();
                var c = cPositions[i];
                nFeatures.push(f);
                if (f.vx == null) {
                    f.vx = c.x - f.x;
                    f.vy = c.y - f.y;
                }
                else {
                    f.vx = smooth*f.vx + (1-smooth)*(c.x - f.x);
                    f.vy = smooth*f.vy + (1-smooth)*(c.y - f.y);
                }
                f.x = c.x;
                f.y = c.y;
            }
        }

        if (errored.length != 0) {
            Main.window.thread.sendMessage(TOther(SLog(
                ' # Feature Disparity Errors (${errored.length})'
            )));
            Main.window.main.sendMessage(TOpenWindow(Thread.current(), ErrorDisplay.displayWindow));
            var errorWin = null;
            GLFWEventLoop.waitEvent('\033[36mWorkHorse\033[m', function (msg:TMessage) {
                return switch (msg) {
                case TOpened(win):
                    errorWin = win;
                    true;
                default: false;
                };
            });
            errorWin.thread.sendMessage(TOther(SData(null,{
                prevFrame: prevFrame,
                currFrame: pFrame,
                errors: errored
            })));
            while (!errorWin.closed) {}

            var del = 0;
            for (e in errored) {
                if (e.status) {
                    var f = e.feature;
                    if (f.vx == null) {
                        f.vx = e.opos[0] - f.x;
                        f.vy = e.opos[1] - f.y;
                    }
                    else {
                        f.vx = smooth*f.vx + (1-smooth)*(e.opos[0] - f.x);
                        f.vy = smooth*f.vy + (1-smooth)*(e.opos[1] - f.y);
                    }
                }
                else {
                    // purge from history
                    for (fc in history) {
                        fc.cnt--;
                        fc.fs.splice(e.ind-del,1);
                        del++;
                    }
                    nFeatures.remove(e.feature);
                }
            }

            Main.window.thread.sendMessage(TOther(SLog(
                '   Lost (${features.length - nFeatures.length})'
            )));
        }

        features = nFeatures;
        featureCount = nFeatures.length;
        history.push({fs:features, cnt:featureCount});

        return false;
    }
    public function new() {
        self = Thread.current();
        state = WSIdle;

        video = null;

        cFrame = pFrame = null;
        processor = null;

        detector = null;

        features = [];
        featureCount = 0;

        prevFrame = null;

        var returnThread:Thread = Thread.readMessage(true);
        GLFWEventLoop.track('\033[36mWorkHorse\033[m ${returnThread}');

        while (true) {
            var msg:TMessage = Thread.readMessage(true);
            GLFWEventLoop.track('\033[36mWorkHorse\033[m ${msg}');
            if (handleAnyState(msg)) break;

            switch (state) {
            case WSIdle:
                handleIdleState(msg);
            case WSTracking:
                switch (msg) {
                case TOther(t): var msg:SMessage = t; switch (msg) {
                case SHalt:
                    state = WSIdle;
                    Main.window.thread.sendMessage(TOther(SLog(
                        ' # Halt Tracking'
                    )));
                    continue;
                default: } default: }
            case WSAnimate:
                switch (msg) {
                case TOther(t): var msg:SMessage = t; switch (msg) {
                case SHalt:
                    state = WSIdle;
                    Main.window.thread.sendMessage(TOther(SLog(
                        ' # Halt Animation'
                    )));
                    continue;
                default: } default: }
            }

            switch (state) {
            case WSTracking:
                if (track()) {
                    state = WSIdle;
                    Main.window.thread.sendMessage(TOther(SLog(
                        ' # Tracking Done (no more frames)'
                    )));
                }
            case WSAnimate:
                if (syncFrame()) {
                    state = WSIdle;
                    Main.window.thread.sendMessage(TOther(SLog(
                        ' # Animation Done'
                    )));
                }
            default:
            }
        }

        GLFWEventLoop.track('@ \033[36mWorkHorse\033[m Terminate -> Main');
        returnThread.sendMessage(TTerminate);

        GLFWEventLoop.track('%\033[36mWorkHorse\033[m TERMINATED');
    }

}

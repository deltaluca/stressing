package;

import gl3font.Font;
import #if cpp cpp #else neko #end.vm.Tls;

class Fonts {

    public static var dejavu(get,never):Font;
    static function get_dejavu() return get("dejavu.sans");

    static var fonts:Tls<Map<String,Font>>;
    static function get(src:String) {
        if (fonts == null) fonts = new Tls<Map<String,Font>>();
        if (fonts.value == null) fonts.value = new Map<String,Font>();
        var fonts = fonts.value;

        if (fonts[src] == null)
            fonts[src] = new Font(src+".dat", src+".png");

        return fonts[src];
    }

}

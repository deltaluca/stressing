package;

import ColourScheme;
import gl3font.*;
import glgui.*;
import goodies.*;
import ogl.*;

class TabMenu {
    public var currentTab(default, null):String;
    public var group(default, null):Group;
    var tabMap:Map<String, PanelButton>;

    public var width    (default, null):Float;
    public var height   (default, null):Float;
    public var tabHeight(default, null):Float;

    static inline var borderThickness = 2;

    public function new(
        width:Float, height:Float,
        tabHeight:Float, currentTab:String
    ) {
        this.width      = width;
        this.height     = height;
        this.tabHeight  = tabHeight;
        this.currentTab = currentTab;
        tabMap = new Map<String, PanelButton>();
        group = new Group()
            .element(new Panel()
                .colour(ColourScheme.border)
                .radius(0)
                .fit([width-borderThickness/2,0,borderThickness,height])
                .commit());
    }

    var cnt = 0;
    public function tab(name:String, fontSize=-1) {
        group.element(tabMap[name] = new PanelButton(true)
            .font(Fonts.dejavu)
            .size(fontSize)
            .text        (GLString.make(name, ColourScheme.face))
            .disabledText(GLString.make(name, ColourScheme.dullFace))

            .fit      ([0,tabHeight*cnt,width-borderThickness/2,tabHeight])
            .radius   (0)
            .thickness(5)

            .colour      (ColourScheme.base)
            .borderColour(ColourScheme.base)
            .pressColour (ColourScheme.base)
            .overColour  (ColourScheme.brightenMask)


            .toggled (name == currentTab)
            .disabled(name == currentTab)

            .press(function (x) if (x) {
                tabMap[currentTab].toggled(false).disabled(false);
                currentTab = name;
                tabMap[name].disabled(true);
            })
            .commit()
        );
        cnt++;
        return this;
    }
}

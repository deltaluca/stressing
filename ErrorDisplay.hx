package;

import glgui.*;
import glgui.GLFWEventLoop;
import glfw3.*;
import gl3font.*;
import ogl.GL;
import ogl.GLM;
import ogl.GLArray;
import cv.Core;
import cv.core.Mat;
import systools.Dialogs;
import goodies.*;
import ColourScheme;
import #if cpp cpp #else neko #end.vm.Thread;

import Main;
import ImageProcessor;
import FeatureDetector;
import FeatureDescriptor;
using glgui.Transform;

typedef InputData = {
    prevFrame: Mat,
    currFrame: Mat,
    errors: Array<{
        status: Bool,
        feature: Feature,
        found: cv.core.Point2D32f,
        error: Float
    }>
};

class ErrorDisplay {

    // Init window settings.
    static inline var windowTitle  = "Lost Features";

    public static function displayWindow() {
    var m:TMessage = Thread.readMessage(true);
    GLFWEventLoop.track('\033[34mErrorDisplay\033[m ${m}');
    switch (m) {
    case TInit(win):

        var data:InputData = null;
        GLFWEventLoop.waitEvent('ErrorDisplay', function (msg:TMessage) {
            return switch (msg) {
            case TOther(ssg): var msg:SMessage = ssg;
                switch (msg) {
                case SData(_, t): data = t; true;
                default: false;
                }
            default: false;
            };
        });

        var actualWidth  = data.prevFrame.cols;
        var actualHeight = 2*data.prevFrame.rows+5;
        var windowWidth  = (actualWidth  > 1800) ? 1800 : actualWidth;
        var windowHeight = (actualHeight > 1000) ? 1000 : actualHeight;

        GLFWEventLoop.track('@ \033[34mErrorDisplay\033[m SetSize -> GLFWEventLoop');
        win.main.sendMessage(TSetSize (win, windowWidth, windowHeight));
        GLFWEventLoop.track('@ \033[34mErrorDisplay\033[m SetTitle -> GLFWEventLoop');
        win.main.sendMessage(TSetTitle(win, windowTitle));
        GLFWEventLoop.track('@ \033[34mErrorDisplay\033[m MakeVisible -> GLFWEventLoop');
        win.main.sendMessage(TMakeVisible(win));

        GLFW.makeContextCurrent(win.window);
        GL.init();

        GL.enable(GL.BLEND);
        GL.blendFunc(GL.SRC_ALPHA, GL.ONE_MINUS_SRC_ALPHA);

        GL.disable(GL.SCISSOR_TEST);

        GL.viewport(0, 0, windowWidth, windowHeight);

        // --------------------------------------------

        var gui = new Gui();
        var glfw = new GLFWGui(win.window);

        // --------------------------------------------

        var videoDisplay1 = new Image()
            .fit([0,0,windowWidth,data.prevFrame.rows]);
        var videoTexture1 = GL.genTextures(1)[0];
        videoDisplay1.texture(videoTexture1);
        var bytes:GLubyteArray = new GLubyteArray(cast data.prevFrame.raw);
        GL.bindTexture(GL.TEXTURE_2D, videoTexture1);
        GL.texImage2D(GL.TEXTURE_2D, 0, GL.LUMINANCE, data.prevFrame.cols, data.prevFrame.rows, 0, GL.LUMINANCE, bytes);
        GL.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MAG_FILTER, GL.LINEAR);
        GL.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MIN_FILTER, GL.LINEAR);

        var videoDisplay2 = new Image()
            .fit([0,data.prevFrame.rows+5,windowWidth,data.prevFrame.rows]);
        var videoTexture2 = GL.genTextures(1)[0];
        videoDisplay2.texture(videoTexture2);
        var bytes:GLubyteArray = new GLubyteArray(cast data.currFrame.raw);
        GL.bindTexture(GL.TEXTURE_2D, videoTexture2);
        GL.texImage2D(GL.TEXTURE_2D, 0, GL.LUMINANCE, data.currFrame.cols, data.currFrame.rows, 0, GL.LUMINANCE, bytes);
        GL.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MAG_FILTER, GL.LINEAR);
        GL.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MIN_FILTER, GL.LINEAR);

        // --------------------------------------------

        var seperated = new Panel()
            .fit([0,data.prevFrame.rows,windowWidth,5])
            .colour(ColourScheme.border)
            .commit();

        // --------------------------------------------

        var liveColour:Vec4 = [1,1,0,1];
        var deadColour:Vec4 = [1,0.5,0,0.6];
        var predColour:Vec4 = [1,0,1,0.6];
        var pathColour:Vec4 = [1,0,1,0.5];

        var liveCols:Array<Vec4> = [
            [1.0,0.0,0.0,0.6],
            [0.0,1.0,0.0,0.6],
            [0.0,0.0,1.0,0.6],
            [1.0,0.0,1.0,0.6],
            [0.0,1.0,1.0,0.6],
            [1.0,1.0,0.0,0.6],
        ];
        var deadCols:Array<Vec4> = [
            [1.0,0.0,0.0,0.4],
            [0.0,0.5,0.0,0.4],
            [0.0,0.0,0.5,0.4],
            [1.0,0.0,0.5,0.4],
            [0.0,1.0,0.5,0.4],
            [1.0,0.5,0.0,0.4],
        ];

        var closest:{status:Bool,feature:Feature,found:cv.core.Point2D32f,error:Float} = null;
        var editing = false;
        var yOff = data.prevFrame.rows+5;

        var baseDrawing = new Drawing();
        for (e in data.errors) {
            var f = e.feature;
            baseDrawing.drawCircle(
                [f.x,f.y],
                2,
                e.status ? liveColour : deadColour
            );
            for (j in 0...f.basics.length) {
                var b = f.basics[j];
                if (b.size > 0)
                    baseDrawing.drawCircle(
                        [f.x,f.y],
                        b.size/2,
                        e.status ? liveCols[j] : deadCols[j]
                    );
                if (b.angle >= 0) {}
            }

            baseDrawing.drawCircle(
                [e.found.x,e.found.y+yOff],
                2,
                predColour
            );
            baseDrawing.drawLine(
                [f.x,f.y],
                [e.found.x,e.found.y+yOff],
                pathColour
            );

            f.x = e.found.x;
            f.y = e.found.y;
        }

        // --------------------------------------------

        var group = new Group();
        var scroll = new Scroll()
            .fit([0,0,windowWidth,windowHeight])
            .element(group)
            .hscroll(actualWidth > windowWidth)
            .vscroll(actualHeight > windowHeight)
            .commit();

        group.element(videoDisplay1);
        group.element(videoDisplay2);
        group.element(new Custom()
        .apply(function (gui:Gui,m:Maybe<Vec2>,proj:Mat3x2,xform:Mat3x2) {
            baseDrawing.begin();
            baseDrawing.setTransform(proj*xform,true);
            baseDrawing.end(true);

            var drawing = gui.drawings();
            drawing.setTransform(proj*xform,true);
            for (e in data.errors) {
                var f = e.feature;
                drawing.drawCircle(
                    [f.x,f.y+yOff],
                    2,
                    e.status ? liveColour : deadColour
                );
                for (j in 0...f.basics.length) {
                    var b = f.basics[j];
                    if (b.size > 0)
                        drawing.drawCircle(
                            [f.x,f.y+yOff],
                            closest != null && closest.feature == f ? b.size : b.size/2,
                            e.status ? liveCols[j] : deadCols[j]
                        );
                    if (b.angle >= 0) {}
                }
            }
            drawing.end();
        }));

        function getFeature(mousePos:Vec2) {
            var closest = null;
            var cdist:Float = 0;
            var cr:Float = 0;
            for (e in data.errors) {
                var f = e.feature;
                var d = Vec2.distance(mousePos, new Vec2([f.x,f.y]));
                var fr = f.basics[0].size;
                if (d > Math.max(4,fr)) continue;
                if (d < cdist || fr < cr || closest == null) {
                    cr = fr;
                    cdist = d;
                    closest = e;
                }
            }
            return closest;
        }

        var mouse = new Mouse()
            .interior(videoDisplay2.internal)
            .mouse(function (pos) {
                var fit = videoDisplay2.getFit();
                if (pos == null) return;
                var pos = pos.extract();
                var proj:Vec2 = [pos.x,pos.y-yOff];
                if (!editing) closest = getFeature(proj);
                else {
                    if (proj.x < 0) proj.x = 0;
                    if (proj.y < 0) proj.y = 0;
                    if (proj.x > fit.z) proj.x = fit.z;
                    if (proj.y > fit.w) proj.y = fit.w;
                    closest.feature.x = proj.x;
                    closest.feature.y = proj.y;
                }
            })
            .press(function (pos, but) {
                if (pos == null) return;
                var pos = pos.extract();
                var proj:Vec2 = [pos.x,pos.y-yOff];
                if (Type.enumEq(but, MouseRight)) {
                    closest = getFeature(proj);
                    if (closest != null)
                        closest.status = !closest.status;
                }
                else if (Type.enumEq(but, MouseLeft)) {
                    closest = getFeature(proj);
                    editing = closest != null;
                }
            })
            .release(function (pos, but, _) {
                if (Type.enumEq(but, MouseLeft))
                    editing = false;
            })
            .commit();
        group.element(mouse);

        // --------------------------------------------

        while (true) {
        var msg:TMessage = Thread.readMessage(true);
        GLFWEventLoop.track('\033[34mErrorDisplay\033[m ${msg}');
        switch (msg) {
        case TTerminate: break;
        case TUpdate(shouldClose):
            if (shouldClose) {
                break;
            }

            // ----------------------------------------

            GL.clear(GL.COLOR_BUFFER_BIT);
            glfw.updateState(gui);

            gui.render(scroll);

            gui.flush();
            GLFW.swapBuffers(win.window);

            // ----------------------------------------

        GLFWEventLoop.track('@ \033[34mErrorDisplay\033[m Continue -> GLFWEventLoop');
        win.main.sendMessage(TContinue(win)); default: }
        }

        // --------------------------------------------

        gui.destroy();
        baseDrawing.destroy();

        GLFWEventLoop.track('@ \033[34mErrorDisplay\033[m CloseWindow -> GLFWEventLoop');
        win.main.sendMessage(TCloseWindow(win));

        GLFWEventLoop.track('%\033[34mErrorDisplay\033[m TERMINATED');
    default: }}
}


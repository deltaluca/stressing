package;

import gl3font.Font;
import #if cpp cpp #else neko #end.vm.Tls;

class Vectors {

    public static var refresh(get,never):Font;
    static function get_refresh() return get("refresh");

    static var vectors:Tls<Map<String,Font>>;
    static function get(src:String) {
        if (vectors == null) vectors = new Tls<Map<String,Font>>();
        if (vectors.value == null) vectors.value = new Map<String,Font>();
        var vectors = vectors.value;

        if (vectors[src] == null)
            vectors[src] = new Font(null, src+".png");

        return vectors[src];
    }

}

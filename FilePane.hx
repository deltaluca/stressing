package;

import glgui.*;
import gl3font.*;
import ogl.*;
import systools.Dialogs;
import ColourScheme;

using glgui.Transform;

class FilePane {

    public var group(default,null):Group;

    public dynamic function onLoadFile  (file:String) {}
    public dynamic function onLoadConfig(file:String) {}
    public dynamic function onSaveConfig(file:String) {}

    var cnt = 0;
    function button(name:String, press:Void->Void) {
        var y = (cnt++)*40;
        return new PanelButton()
            .font(Fonts.dejavu)
            .thickness(3)
            .radius(15)
            .text(GLString.make(name, ColourScheme.face))
            .size(15)
            .fit([90,90+y,100,36])
            .colour(ColourScheme.base)
            .overColour(ColourScheme.brightenMask)
            .press(function (_) press())
            .commit();
    }

    public function new() {
        group = new Group()
        .element(button("Load Video", function () {
            var filters:FILEFILTERS = {
                count: 1,
                descriptions: ["Video Files"],
                extensions: ["*.avi;*.mpeg;*.m4v;*.mp4"]
            };
            var result = Dialogs.openFile("Select a Video file", "", filters);
            if (result != null) onLoadFile(result[0]);
        }))
        .element(button("Load Config", function () {
            var filters:FILEFILTERS = {
                count: 1,
                descriptions: ["Stress Config Files"],
                extensions: ["*.json"]
            };
            var result = Dialogs.openFile("Select a Config file", "", filters);
            if (result != null) onLoadConfig(result[0]);
        }))
        .element(button("Save Config", function () {
            var result = Dialogs.saveFile("Select Config save location", "", Sys.getCwd());
            if (result != null) onSaveConfig(result);
        }));
    }

}

package;

import glgui.*;
import glfw3.*;
import gl3font.*;
import ogl.GLM;
import systools.Dialogs;
import goodies.*;
import ColourScheme;
import ConfigPane;
import ImageProcessor;

using glgui.Transform;

typedef JProcessor = {
    type : String
};
typedef JPMedian = { > JProcessor,
    kernel : Int
};
typedef JPGaussian = { > JProcessor,
    kernel : Int
};
typedef JPBilateral = { > JProcessor,
    diameter : Int,
    colour   : Float,
    space    : Float
};
typedef JPHistogram = JProcessor;
typedef JPAdaptive = { > JProcessor,
    nX    : Int,
    nY    : Int,
    limit : Float
};
typedef JPCustom = { > JProcessor,
    file : String
};

class ProcessorPane {
    public static function create(width:Float, height:Float) {
        var pane = new ConfigPane<ImageProcessor>(width, height);
        pane.addButton("Median Blur",    createMedian   .bind(_,null));
        pane.addButton("Gaussian Blur",  createGaussian .bind(_,null));
        pane.addButton("Bilateral Blur", createBilateral.bind(_,null));
        pane.addButton("Histrogram",     createHistogram.bind(_,null));
        pane.addButton("Adaptive Hist.", createAdaptive .bind(_,null));
        pane.addButton("Custom",         createCustom   .bind(_,null));
        pane.graph.multi = function (xs)
            return new MultiProcessor(xs);
        return pane;
    }

    public static function parse(pane:ConfigPane<ImageProcessor>, ps:Array<JProcessor>) {
        var cur = pane.graph.root;
        for (p in ps) {
            var box =
            switch (p.type) {
            case "Median":    createMedian   (pane, cast p);
            case "Gaussian":  createGaussian (pane, cast p);
            case "Bilateral": createBilateral(pane, cast p);
            case "Histogram": createHistogram(pane, cast p);
            case "Adaptive":  createAdaptive (pane, cast p);
            case "Custom":    createCustom   (pane, cast p);
            case x: throw 'Unknown processor type "$x"'; null;
            }
            cur.forth = box;
            cur = box;
        }
    }

    static function createMedian(pane:ConfigPane<ImageProcessor>, ?init:JPMedian=null) {
        var box = Box.createBox(pane.graph, 140, 60, "Median Blur");
        var kernel = ConfigPane.genModifier("Kernel Size", 13*3);
        kernel.group.position([140-45,33]);
        kernel.inp.maxChars(3)
              .allowed(~/[0-9]/)
              .text(GLString.make(init == null ? "5" : Std.string(init.kernel), ColourScheme.base));
        box.group.element(kernel.group);
        box.build = function () {
            var kernel = Std.parseInt(kernel.inp.getText().toString());
            if (kernel%2 != 1)
                throw "Median Blur : Kernel size must be an odd number";
            return new MedianProcessor(kernel);
        }
        box.write = function ():JPMedian {
            return {
            type   : "Median",
            kernel : Std.parseInt(kernel.inp.getText().toString())
            };
        }
        return box;
    }

    static function createGaussian(pane:ConfigPane<ImageProcessor>, ?init:JPGaussian=null) {
        var box = Box.createBox(pane.graph, 140, 60, "Gaussian Blur");
        var kernel = ConfigPane.genModifier("Kernel Size", 13*3);
        kernel.group.position([140-45,33]);
        kernel.inp.maxChars(3)
                  .allowed(~/[0-9]/)
                  .text(GLString.make(init == null ? "5" : Std.string(init.kernel), ColourScheme.base));
        box.group.element(kernel.group);
        box.build = function () {
            var kernel = Std.parseInt(kernel.inp.getText().toString());
            if (kernel%2 != 1)
                throw "Gaussian Blur : Kernel size must be an odd number";
            return new GaussianProcessor(kernel);
        }
        box.write = function ():JPGaussian {
            return {
            type   : "Gaussian",
            kernel : Std.parseInt(kernel.inp.getText().toString())
            };
        }
        return box;
    }

    static function createBilateral(pane:ConfigPane<ImageProcessor>, ?init:JPBilateral=null) {
        var box = Box.createBox(pane.graph, 140, 96, "Bilateral Blur");
        var diameter = ConfigPane.genModifier("Diameter", 13*3);
        diameter.group.position([125-45,33]);
        diameter.inp.maxChars(3)
                    .allowed(~/[0-9]/)
                    .text(GLString.make(init == null ? "5" : Std.string(init.diameter), [0,0,0,1]));
        box.group.element(diameter.group);
        var colour = ConfigPane.genModifier("σ-colour", 13*4);
        colour.group.position([125-45,33+18]);
        colour.inp.maxChars(9)
                  .allowed(~/[0-9.eE+\-]/)
                  .text(GLString.make(init == null ? "0.01" : Std.string(init.colour), [0,0,0,1]));
        box.group.element(colour.group);
        var space = ConfigPane.genModifier("σ-space", 13*4);
        space.group.position([125-45,33+18*2]);
        space.inp.maxChars(9)
                 .allowed(~/[0-9.eE+\-]/)
                 .text(GLString.make(init == null ? "0.01" : Std.string(init.space), [0,0,0,1]));
        box.group.element(space.group);
        box.build = function () {
            var diameter = Std.parseInt(diameter.inp.getText().toString());
            var colour   = Std.parseFloat(colour.inp.getText().toString());
            var space    = Std.parseFloat(space.inp.getText().toString());
            return new BilateralProcessor(diameter, colour, space);
        }
        box.write = function ():JPBilateral {
            return {
            type     : "Bilateral",
            diameter : Std.parseInt(diameter.inp.getText().toString()),
            colour   : Std.parseFloat(colour.inp.getText().toString()),
            space    : Std.parseFloat(space.inp.getText().toString())
            };
        }
        return box;
    }

    static function createHistogram(pane:ConfigPane<ImageProcessor>, ?init:JPHistogram=null) {
        var box = Box.createBox(pane.graph, 140, 50, "Histogram\nEqualisation", false, 0);
        box.build = function() return new HistogramProcessor();
        box.write = function ():JPHistogram {
            return {
            type : "Histogram"
            };
        }
        return box;
    }

    static function createAdaptive(pane:ConfigPane<ImageProcessor>, ?init:JPAdaptive=null) {
        var box = Box.createBox(pane.graph, 140, 110, "Adaptive\nHist. Eq.", false, 40);
        var nX = ConfigPane.genModifier("num-x", 13*3);
        nX.group.position([120-48,48]);
        nX.inp.maxChars(3)
              .allowed(~/[0-9]/)
              .text(GLString.make(init == null ? "5" : Std.string(init.nX), [0,0,0,1]));
        box.group.element(nX.group);
        var nY = ConfigPane.genModifier("num-y", 13*3);
        nY.group.position([120-48,48+18]);
        nY.inp.maxChars(3)
              .allowed(~/[0-9]/)
              .text(GLString.make(init == null ? "5" : Std.string(init.nY), [0,0,0,1]));
        box.group.element(nY.group);
        var limit = ConfigPane.genModifier("limit", 13*4);
        limit.group.position([120-48,48+18*2]);
        limit.inp.maxChars(9)
              .allowed(~/[0-9.eE+\-]/)
              .text(GLString.make(init == null ? "0.05" : Std.string(init.limit), [0,0,0,1]));
        box.group.element(limit.group);
        box.build = function() {
            var nX    = Std.parseInt(nX.inp.getText().toString());
            var nY    = Std.parseInt(nY.inp.getText().toString());
            var limit = Std.parseFloat(limit.inp.getText().toString());
            return new AdaptiveHistogramProcessor(nX, nY, limit);
        }
        box.write = function ():JPAdaptive {
            return {
            type  : "Adaptive",
            nX    : Std.parseInt(nX.inp.getText().toString()),
            nY    : Std.parseInt(nY.inp.getText().toString()),
            limit : Std.parseFloat(limit.inp.getText().toString())
            };
        }
        return box;
    }

    static function createCustom(pane:ConfigPane<ImageProcessor>, ?init:JPCustom=null) {
        var box = Box.createBox(pane.graph, 170,60, "Custom");
        var txt = new TextInput()
            .fileInput(true)
            .fit([4+50+8,30,106,22])
            .size(13)
            .colour(ColourScheme.base)
            .text(GLString.make(init == null ? "<select>" : init.file, ColourScheme.base))
            .font(Fonts.dejavu)
            .commit();
        var but = new PanelButton()
            .fit([7,31,50,22])
            .colour(ColourScheme.face)
            .borderColour(ColourScheme.border)
            .pressColour(ColourScheme.brightenMask)
            .size(13)
            .radius(6)
            .thickness(2)
            .text(GLString.make("Select", ColourScheme.base))
            .font(Fonts.dejavu)
            .press(function (_) {
                var file = Dialogs.openFile(
                    "Select File",
                    "Select file for custom processor",
                    {
                        count: 1,
                        descriptions: ["Haxe (hscript) file"],
                        extensions: ["*.hx"]
                    }
                );
                if (file != null) {
                    txt.text(GLString.make(file[0], ColourScheme.base));
                    txt.gotoEnd();
                    txt.commit();
                }
            })
            .commit();
        box.group.element(txt);
        box.group.element(but);
        box.build = function () {
            var path = ""+txt.getText().toString();
            if (!sys.FileSystem.exists(path))
                throw '"$path" is not a valid file path';
            if (sys.FileSystem.isDirectory(path))
                throw '"$path" is not a file';
            try {
                return new CustomProcessor(
                    Config.loadCustom(path)
                );
            }
            catch(e:Dynamic) {
                throw 'hscript parse error: $e';
            }
        }
        box.write = function ():JPCustom {
            return {
            type : "Custom",
            file : txt.getText().toString()
            };
        }
        return box;
    }
}

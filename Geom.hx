package;

import ogl.GL;

// triangle in a triangulation.
class TriTriangle {
    // points of triangle
    public var a:Vec2;
    public var b:Vec2;
    public var c:Vec2;

    // inner triangles
    public var inner:Array<TriTriangle>;

    public function new() {
        inner = [];
    }

    static inline function det3(a:Vec2, b:Vec2, c:Vec2):Float {
        var al = a.x*a.x + a.y*a.y;
        var bl = b.x*b.x + b.y*b.y;
        var cl = c.x*c.x + c.y*c.y;
        return a.x * (b.y * cl - bl * c.y)
             + b.x * (c.y * al - cl * a.y)
             + c.x * (a.y * bl - al * b.y);
    }
    public static function inside(p:Vec2, a:Vec2, b:Vec2, c:Vec2):Bool {
        return (det3(b, c, p) - det3(a, c, p)
              + det3(a, b, p) - det3(a, b, c)) <= 0;
    }
}

typedef Triangulation = Array<TriTriangle>;

class Geom {
    public static function triangulate(points:Array<Vec2>):Triangulation {
    }
}

package;

import ogl.GLM;

class ColourScheme {
    public static var base              = [0.1,0.1,0.1,1.0];
    public static var face              = [0.9,0.9,0.9,1.0];
    public static var brightFace        = [1.0,1.0,1.0,1.0];
    public static var dullFace          = [0.4,0.4,0.4,1.0];
    public static var border            = [0.5,0.5,0.5,1.0];

    public static var brightenMask      = [1.0,1.0,1.0,0.15];
    public static var darkenMask        = [0.0,0.0,0.0,0.15];

    public static var bottomJoint       = [0.0,1.0,0.0,1.0];
    public static var topJoint          = [1.0,0.0,0.0,1.0];
    public static var sideJoint         = [0.0,0.0,1.0,1.0];

    public static var partialLink       = [1.0,1.0,1.0,0.75];
    public static var bottomLink        = [0.5,1.0,0.5,1.0];
    public static var topLink           = [1.0,0.5,0.5,1.0];
    public static var sideLink          = [0.5,0.5,1.0,1.0];

    public static var tabColour         = [0.5,0.9,0.5,0.9];
    public static var disabledTabColour = [1.0,1.0,1.0,0.4];

    public static var fileColour        = [0.9,0.9,0.5,1.0];
}

package;

import cv.core.Image;
import cv.core.Mat;
import cv.highgui.Capture;
import cv.Core;
import cv.HighGUI;
import cv.ImgProc;
import goodies.Maybe;
import Video;
import ogl.GL;
import ogl.GLM;
import ogl.GLArray;

class CVVideo implements Video {
    public var width:Int;
    public var height:Int;
    public var fname:String;

    var capture:Capture;
    var frameCount:Int;
    var currentFrame:Int;

    var store:VideoStore;

    public function new(fname:String) {
        this.fname = fname;

        capture = HighGUI.captureFromFile(fname);
        width = Std.int(HighGUI.getCaptureProperty(capture, HighGUI.CAP_PROP_FRAME_WIDTH));
        height = Std.int(HighGUI.getCaptureProperty(capture, HighGUI.CAP_PROP_FRAME_HEIGHT));

        frameCount = Std.int(HighGUI.getCaptureProperty(capture, HighGUI.CAP_PROP_FRAME_COUNT));
        currentFrame = 0;
        store = new VideoStore();
        buffer();
    }

    var buffered = false;
    public function buffer() {
        while (bufferNext()) {}
        return this;
    }

    public function bufferNext() {
        if (buffered) return false;

        var image = HighGUI.queryFrame(capture);
        if (image == null) {
            buffered = true;
            return false;
        }

        var mat = Core.createMat(height, width, Core._8U);
        ImgProc.cvtColor(image, mat, ImgProc.RGB2GRAY);
        store.push(mat);

        return true;
    }

    public function restart() {
        currentFrame = 0;
    }

    public function frame(n:Int) return store.get(n);

    public function update():Maybe<Mat> {
        bufferNext();
        bufferNext();
        var ret = frame(currentFrame);
        if (ret != null)
            currentFrame++;
        return ret;
/*
        var image = HighGUI.queryFrame(capture);
        if (image == null) return null;

        var mat = Core.createMat(height, width, Core._8U);
        ImgProc.cvtColor(image, mat, ImgProc.RGB2GRAY);
        store.push(mat);
        currentFrame++;
        return mat; */
    }
}

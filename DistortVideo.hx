package;

import cv.core.Mat;
import cv.Core;
import ogl.GL;
import ogl.GLM;
import ogl.GLArray;

class DistortVideo implements Video {
    public var width:Int;
    public var height:Int;
    public var fname:String;
    var currentFrame:Int;

    public function new() {
        fname = "<>";
        width = 550;
        height = 400;
        currentFrame = 0;
    }

    public function restart() {
        currentFrame = 0;
    }
    public function frame(n:Int) return compute(n);
    public function update() {
        return compute(currentFrame++);
    }

    function compute(n:Int):Mat {
        var mat = Core.createMat(400, 550, Core._8U);
        Core.set(mat, Core.scalar(0));
        for (i in 0...10) {
            var dy = Std.int(Math.sin((currentFrame+i)/10)*40);
            Core.circle(mat, Core.point(55 + currentFrame + i*20,dy+50),3,Core.scalar(0xff),-1);
            Core.circle(mat, Core.point(55 + currentFrame + i*20,-dy+150 +currentFrame),3,Core.scalar(0xff),-1);
        }
        return mat;
    }
}

package;

import cv.Core;
import cv.ImgProc;
import cv.NonFree;
import cv.Features2D;
import cv.core.*;
import cv.nonfree.*;
import cv.features2d.*;

import FeatureDetector;

interface ComplexDescriptor {
    public function compare(x:ComplexDescriptor):Float;
    public function magnitude():Float;
}

interface DescriptorExtractor {
    public function extract(src:Mat, features:Array<Feature>):Void;
}

class VectorDescriptor implements ComplexDescriptor {
    // subset of description matrix [offset,offset+length)
    var description:Array<Float>;
    var offset:Int;
    var length:Int;

    public function new(description, offset, length) {
        this.description = description;
        this.offset = offset;
        this.length = length;
    }

    public function magnitude() {
        var ret = 0.0;
        for (i in offset...offset+length) ret += description[i]*description[i];
        return Math.sqrt(ret);
    }
    public function compare(x:ComplexDescriptor) {
        var y:VectorDescriptor = cast x;
        return 0;
    }
}

class SurfExtractor implements DescriptorExtractor {
    public var surf:SURF;
    public function new(surf:SURF) {
        this.surf = surf;
    }
    public function extract(src:Mat, features:Array<Feature>):Void {
        var keypoints = [];
        for (f in features) {
            for (b in f.basics) {
                keypoints.push(KeyPoint.make(f.x, f.y, b.size, b.angle, b.response, b.octave));
            }
        }
        var descriptor:Array<Float> = [];
        surf.detect(src, null, keypoints, descriptor, true);
        var i = 0;
        var size = surf.extended ? 128 : 64;
        for (f in features) {
            for (b in f.basics) {
                f.complex.push(new VectorDescriptor(descriptor, i*size, size));
                i++;
            }
        }
    }
}

class SiftExtractor implements DescriptorExtractor {
    public var sift:SIFT;
    public function new(sift:SIFT) {
        this.sift = sift;
    }
    public function extract(src:Mat, features:Array<Feature>) {
        var keypoints = [];
        for (f in features) {
            for (b in f.basics) {
                keypoints.push(KeyPoint.make(f.x, f.y, b.size, b.angle, b.response, b.octave));
            }
        }
        var descriptor:Array<Float> = [];
        sift.detect(src, null, keypoints, descriptor, true);
        var i = 0;
        for (f in features) {
            for (b in f.basics) {
                f.complex.push(new VectorDescriptor(descriptor, i*128, 128));
                i++;
            }
        }
    }
}

class BriskExtractor implements DescriptorExtractor {
    public var brisk:BRISK;
    public function new(brisk:BRISK) {
        this.brisk = brisk;
    }
    public function extract(src:Mat, features:Array<Feature>) {
        var keypoints = [];
        for (f in features) {
            for (b in f.basics) {
                keypoints.push(KeyPoint.make(f.x, f.y, b.size, b.angle, b.response, b.octave));
            }
        }
        var descriptor:Array<Float> = [];
        brisk.detect(src, null, keypoints, descriptor, true);
        var i = 0;
        for (f in features) {
            for (b in f.basics) {
                f.complex.push(new VectorDescriptor(descriptor, i*64, 64));
                i++;
            }
        }
    }
}

class CustomExtractor implements DescriptorExtractor {
    public var n:Int;
    public var fun:Mat->KeyPoint->Array<Dynamic>;
    public function new(n:Int, fun:Mat->KeyPoint->Array<Dynamic>) {
        this.n = n;
        this.fun = fun;
    }
    public function extract(src:Mat, features:Array<Feature>) {
        var keypoints = [];
        for (f in features) {
            for (b in f.basics) {
                keypoints.push(KeyPoint.make(f.x, f.y, b.size, b.angle, b.response, b.octave));
            }
        }
        var i = 0;
        for (f in features) {
            for (b in f.basics) {
                var descriptor = fun(src, keypoints[i]);
                var d2:Array<Float> = [];
                for (d in descriptor) d2.push(d);
                f.complex.push(new VectorDescriptor(d2, 0, n));
                i++;
            }
        }
    }
}

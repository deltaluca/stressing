package;

import glgui.*;
import glfw3.*;
import gl3font.*;
import ogl.GLM;
import systools.Dialogs;
import goodies.*;
import ColourScheme;
import Main;
import ColourScheme;
import FeatureDescriptor;

using glgui.Transform;

typedef JDescriptor = {
    type : String
};

class DescriptorPane {
    public var group:Group;

    public function new() {}

    public static function create(width:Float, height:Float) {
        var pane = new DescriptorPane();
        pane.group = new Group();
        return pane;
    }

    public function build():Maybe<DescriptorExtractor> {
        return null;
    }

    public function write():JDescriptor {
        return null;
    }

    public function parse(ps:JDescriptor) {
    }
}

package;

import glgui.*;
import glfw3.*;
import gl3font.*;
import ogl.GLM;
import systools.Dialogs;
import goodies.*;
import ColourScheme;

using glgui.Transform;

enum LinkType {
    LinkForth;
    LinkBack;
    LinkSide;
}

class Box<T> {
    static var nextId = 0;
    public var id:Int;
    public var data:Dynamic;

    var graph:ConfigGraph<T>;
    public var group:Group;

    // bounds of main body.
    public var width:Float;
    public var height:Float;

    public var forth:Maybe<Box<T>>;
    public var side:Maybe<Box<T>>;

    public var backPos :Vec2;
    public var forthPos:Vec2;
    public var sidePos :Vec2;

    function xform(x:Vec2):Vec2 {
        var p = group.getPosition();
        return Mat3x2.translate(p.x, p.y) * x;
    }
    public function backPosition () return xform(backPos);
    public function forthPosition() return xform(forthPos);
    public function sidePosition () return xform(sidePos);

    public var build:Void->T;
    public var write:Void->Dynamic;

    function new(graph:ConfigGraph<T>) {
        group = new Group();
        this.graph = graph;
        graph.add(this);
        id = nextId++;
    }

    static public function createRoot<T>(graph:ConfigGraph<T>, size:Float) {
        var box = new Box<T>(graph);
        box.genBottom(size, size);
        box.genBody(size, size, false);
        return box;
    }

    static public function createBox<T>(graph:ConfigGraph<T>, width:Float, height:Float, title:String, side=false, barHeight=25.0) {
        var box = new Box<T>(graph);
        box.genBottom(width, height);
        box.genTop   (width, height);
        if (side) box.genSide(width, height);
        box.genBody(title, width, height, barHeight!=0, barHeight);
        return box;
    }

    function genTop   (width:Float, height:Float)
        backPos  = genLink(width/2, 0,      ColourScheme.topJoint,    LinkBack);
    function genBottom(width:Float, height:Float)
        forthPos = genLink(width/2, height, ColourScheme.bottomJoint, LinkForth);
    function genSide  (width:Float, height:Float)
        sidePos  = genLink(width, height/2, ColourScheme.sideJoint,   LinkSide);

    function genLink(x:Float, y:Float, col:Vec4, link:LinkType) {
        var t;
        group.element(t = new Panel()
            .fit([x-10,y-10,20,20])
            .radius(10)
            .colour(ColourScheme.face)
            .commit()
        )
        .element(new Panel()
            .fit([x-9,y-9,18,18])
            .radius(9)
            .colour(col)
            .commit()
        )
        .element(new Mouse()
            .interior(t.internal)
            .enter(function () graph.enter(this, link))
            .exit (function () graph.exit (this, link))
            .press(function (_,but) {
                if (!Type.enumEq(but, MouseLeft)) return;
                graph.startLink(this, link);
            })
            .release(function (_,but,_) {
                if (!Type.enumEq(but, MouseLeft)) return;
                graph.endLink();
            })
            .commit()
        );
        return [x,y];
    }

    function genBody(title:String=null, width:Float, height:Float, bar:Bool, barHeight=0.0) {
        this.width = width;
        this.height = height;
        var r = Math.min(20, Math.min(width,height)/2);
        var dragElement:Dynamic;
        group.element(dragElement = new Panel()
            .fit([0,0,width,height])
            .colour(ColourScheme.base)
            .radius(r)
            .commit())
        .element(new Panel()
            .fit([2,2,width-4,height-4])
            .colour(ColourScheme.face)
            .radius(r-3)
            .commit());
        if (bar)
            group.element(new Panel()
                .fit([2,barHeight,width-4,2])
                .colour(ColourScheme.border)
                .radius(0)
                .commit());

        if (title != null) {
            group.element(dragElement = new Text()
                .fit([24,2,width-48, bar ? barHeight-1 : height-6])
                .text(GLString.make(title, ColourScheme.base))
                .font(Fonts.dejavu)
                .halign(TextAlignCentre)
                .valign(TextAlignCentre)
                .commit());
        }

        var offset:Vec2;
        var drag:Bool = false;
        group.element(new Mouse()
            .fit(dragElement.bounds())
            .press(function (_, but) {
                graph.bringToFront(this);
                if (!Type.enumEq(but, MouseLeft)) return;
                var mpos:Vec2 = GLFW.getCursorPos(Main.window.window);
                offset = mpos;
                drag = true;
            })
            .release(function (_, but, _) {
                if (!Type.enumEq(but, MouseLeft)) return;
                drag = false;
            })
            .mouse(function (_) {
                if (!drag) return;
                var mpos:Vec2 = GLFW.getCursorPos(Main.window.window);
                var pos = group.getPosition() + (mpos - offset);
                offset = mpos;
                group.position(pos);
            })
        );

        if (title != null)
            group.element(new PanelButton()
                .fit([6,9,14,14])
                .thickness(0)
                .colour([1,0,0,1])
                .radius(7)
                .text(GLString.make("X", [1,1,1,1]))
                .size(16)
                .font(Fonts.dejavu)
                .press(function (_) graph.remove(this))
                .commit());
    }
}

class ConfigGraph<T> {
    public var root:Box<T>;
    public var boxes:Array<Box<T>>;

    public var group(default,null):Group;

    var pressed:Maybe<Box<T>>;
    var linkType:Maybe<LinkType> = null;
    var overs:Array<{box:Box<T>,type:LinkType}>;

    public function clear() {
        while (boxes.length > 0) remove(boxes[0]);
        root = Box.createRoot(this, 30);
    }

    public function new() {
        boxes = [];
        overs = [];
        group = new Group()
        .element(new Custom()
        .apply(function (gui, mpos, proj, xform) {
            var drawing = gui.drawings();
            drawing.setTransform(proj * xform);
            drawing.swapFills();
            for (box in boxes) {
                if (box.forth != null) {
                    var p:Vec2 = box.forthPosition();
                    var q:Vec2 = box.forth.extract().backPosition();
                    var d:Vec2 = [p.y-q.y, q.x-p.x];
                    d *= 2/Math.sqrt(d.x*d.x+d.y*d.y);
                    drawing.pushVertex(p+d, ColourScheme.bottomLink);
                    drawing.pushVertex(p-d, ColourScheme.bottomLink);
                    drawing.pushVertex(q-d, ColourScheme.topLink);

                    drawing.pushVertex(p+d, ColourScheme.bottomLink);
                    drawing.pushVertex(q-d, ColourScheme.topLink);
                    drawing.pushVertex(q+d, ColourScheme.topLink);
                }
                if (box.side != null) {
                    var p:Vec2 = box.sidePosition();
                    var q:Vec2 = box.side.extract().backPosition();
                    var d:Vec2 = [p.y-q.y, q.x-p.x];
                    d *= 2/Math.sqrt(d.x*d.x+d.y*d.y);
                    drawing.pushVertex(p+d, ColourScheme.sideLink);
                    drawing.pushVertex(p-d, ColourScheme.sideLink);
                    drawing.pushVertex(q-d, ColourScheme.topLink);

                    drawing.pushVertex(p+d, ColourScheme.sideLink);
                    drawing.pushVertex(q-d, ColourScheme.topLink);
                    drawing.pushVertex(q+d, ColourScheme.topLink);
                }
            }

            if (pressed != null && mpos != null) {
                var pressed = pressed.extract();
                drawing.drawLine(
                    switch (linkType.extract()) {
                    case LinkForth: pressed.forthPosition();
                    case LinkBack : pressed.backPosition();
                    case LinkSide : pressed.sidePosition();
                    }, mpos.extract(), ColourScheme.partialLink);
            }
        }));
        root = Box.createRoot(this, 30);
    }

    public function bringToFront(x:Box<T>) {
        group.bringToFront(x.group);
    }
    public function add(x:Box<T>) {
        boxes.push(x);
        group.element(x.group);
    }
    public function remove(x:Box<T>) {
        boxes.remove(x);
        group.removeElement(x.group);
        for (b in boxes) {
            if (b.forth == x) b.forth = null;
            if (b.side  == x) b.side  = null;
        }
    }

    public function startLink(x:Box<T>, link:LinkType) {
        pressed = x;
        linkType = link;
    }
    public function endLink() {
        var link = linkType.extract();
        var pressed = pressed.extract();
        if (overs.length == 0) {
            if (Type.enumEq(link, LinkForth)) pressed.forth = null;
            if (Type.enumEq(link, LinkSide )) pressed.side  = null;
            this.pressed  = null;
            this.linkType = null;
            return;
        }
        var olink = overs.pop();
        if      (Type.enumEq(link, LinkForth)) pressed.forth = olink.box;
        else if (Type.enumEq(link, LinkBack ))
            if (Type.enumEq(olink.type, LinkForth))
                 olink.box.forth = pressed;
            else olink.box.side  = pressed;
        else pressed.side = olink.box;
        this.pressed  = null;
        this.linkType = null;
    }

    function compatibleLink(from:LinkType, to:LinkType)
        return switch (from) {
        case LinkForth: Type.enumEq(to, LinkBack);
        case LinkSide:  Type.enumEq(to, LinkBack);
        case LinkBack:  !Type.enumEq(to, LinkBack);
        };
    public function enter(x:Box<T>, link:LinkType) {
        if (this.linkType != null && compatibleLink(this.linkType.extract(), link) && x != pressed.extract())
            overs.push({box:x, type:link});
    }
    public function exit(x:Box<T>, link:LinkType) {
        if (this.linkType != null && compatibleLink(this.linkType.extract(), link) && x != pressed.extract()) {
            var i = 0;
            while (i < overs.length) {
                if (overs[i].box == x) {
                    overs[i] = overs[overs.length - 1];
                    overs.pop();
                    break;
                }
            }
        }
    }

    public function format() {
        // Arranges elements connected to root only.
        function chainSize(root:Box<T>):{boxes:Array<Box<T>>, size:Vec2} {
            var size:Vec2 = [0,0];
            var cur:Box<T> = root;
            var boxes = [];
            while (cur != null) {
                boxes.push(cur);
                if (cur.width > size.x) size.x = cur.width;
                if (cur.height > size.y) size.y = cur.height;
                cur = untyped cur.forth;
            }
            return {boxes:boxes, size:size};
        }

        var gap:Float = 30;
        function position(root:Box<T>, x:Float, y:Float):Vec2 {
            var rootChain = chainSize(root);
            var sidex = x + rootChain.size.x + gap;
            for (box in rootChain.boxes) {
                box.group.position([x + (rootChain.size.x-box.width)/2, y]);
                y += gap + box.height;
            }

            rootChain.boxes.reverse();
            for (box in rootChain.boxes) {
                if (box.side != null)
                    sidex += gap + position(box.side.extract(), sidex, box.group.getPosition().y + box.height*3/4).x;
            }

            return rootChain.size;
        }
        position(root, 0, 0);
    }

    public var multi:Array<T>->T;

    public function write(root:Box<T>=null) {
        var visited = new Map<Int,Bool>();
        var results = [];
        var cur:Box<T> =
            if (root != null) root;
            else this.root;
        while (cur != null) {
            if (visited.exists(cur.id)) throw "Infinite loop in configuration graph";
            visited[cur.id] = true;
            if (cur != this.root)
                results.push(cur.write());
            cur = untyped cur.forth;
        }
        return results;
    }

    public function build(root:Box<T>=null):Maybe<T> {
        try {
            var visited = new Map<Int,Bool>();
            var results = [];
            var cur:Box<T> =
                if (root != null) root;
                else this.root;
            while (cur != null) {
                if (visited.exists(cur.id)) throw "Infinite loop in configuration graph";
                visited[cur.id] = true;
                if (cur != this.root)
                    results.push(cur.build());
                cur = untyped cur.forth;
            }
            return cast multi(results);
        }
        catch(e:Dynamic) {
            Dialogs.message("Error!", Std.string(e), true);
            return null;
        }
    }
}

class ConfigPane<T> {
    public var group(default,null):Group;
    public var pane (default,null):Scroll<Group>;
    public var graph(default,null):ConfigGraph<T>;

    var width:Float;
    public function new(width:Float, height:Float) {
        this.width = width;
        graph = new ConfigGraph<T>();
        pane = new Scroll<Group>()
            .fit([0,60,width,height-60])
            .hscroll(true)
            .vscroll(true)
            .element(graph.group)
            .commit();
        group = new Group()
        .element(new Panel()
            .fit([0,60,width,height-60])
            .colour(ColourScheme.base)
            .commit())
        .element(pane)
        .element(new Panel()
            .fit([0,60-2,width,2])
            .colour(ColourScheme.border)
            .radius(0)
            .commit());

        buttonWidth = width / 5;
        buttonHeight = 30;
        x = y = 0;
    }

    var buttonWidth:Float;
    var buttonHeight:Float;
    var x:Int;
    var y:Int;
    public function addButton(name:String, gen:ConfigPane<T>->Box<T>) {
        group.element(new PanelButton()
            .fit([x*buttonWidth, y*buttonHeight, buttonWidth, buttonHeight])
            .font(Fonts.dejavu)
            .text(GLString.make(name, ColourScheme.face))
            .radius(5)
            .thickness(2)
            .press(function (_) {
                var box = gen(this);
                box.group.position(getMiddle(pane, box.group));
            })
            .commit());
        x++;
        if (x >= 5) {
            y++;
            x = 0;
        }
    }

    static function getMiddle(scroll:Dynamic, x:Dynamic):Vec2 {
        var f:Vec4 = scroll.getFit();
        var s:Vec2 = scroll.getScroll();
        var g:Vec4 = x.bounds();
        var p:Vec2 = [f.z/2 - g.z/2, f.w/2 - g.w/2];
        return p - s;
    }

    static public function genModifier(name:String, space:Float, ?col:Vec4) {
        if (col == null) col = ColourScheme.base;
        var group = new Group();
        var text = new Text()
            .font(Fonts.dejavu)
            .text(GLString.make(name+":", col))
            .size(15)
            .halign(TextAlignRight)
            .valign(TextAlignTop)
            .position([-2,0])
            .commit();
        var fit = text.bounds().extract();
        var pan = new Panel()
            .fit([2,0,space,fit.w])
            .radius(5)
            .colour(ColourScheme.brightFace)
            .commit();
        var inp = new TextInput()
            .font(Fonts.dejavu)
            .size(15)
            .fit(pan.getFit() + new Vec4([4.5,3,-6,0]))
            .colour(ColourScheme.base)
            .multiline(false)
            .commit();
        group.element(text)
             .element(pan)
             .element(inp);
        return { group: group, inp: inp };
    }

    static public function genCheckModifier(name:String) {
        var group = new Group();
        var text = new Text()
            .font(Fonts.dejavu)
            .text(GLString.make(name+":", ColourScheme.base))
            .size(15)
            .halign(TextAlignRight)
            .valign(TextAlignTop)
            .position([-2,0])
            .commit();
        var check = new PanelButton(true)
            .fit([6,1,16,16])
            .radius(0)
            .size(15)
            .font(Fonts.dejavu)
            .text(GLString.make("X", [0,0,0,1]))
            .disabledText(GLString.make("X", ColourScheme.face))
            .colour      (ColourScheme.brightFace)
            .borderColour(ColourScheme.face)
            .overColour  (ColourScheme.darkenMask)
            .pressColour (ColourScheme.border)
            .commit();
        group.element(text)
             .element(check);
        return { group: group, check: check };
    }

    static public function genRefresh(press:Void->Void) {
        var group = new Group()
        .element(new PanelButton()
            .fit([-18,-18,36,36])
            .radius(18)
            .thickness(2)
            .press(function (_) press())
            .commit())
        .element(new Vector()
            .image(Vectors.refresh)
            .fit([-12,-12,24,24])
            .colour([1,1,1,1])
            .commit());
        return group;
    }
}

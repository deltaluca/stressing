package;

import cv.Core;
import cv.ImgProc;
import cv.NonFree;
import cv.Features2D;
import cv.core.*;
import cv.nonfree.*;
import cv.features2d.*;
import ogl.GLM;

import FeatureDescriptor;

// Shadows cv.features2d.KeyPoint, minus position of feature
// stored seperately.
class BasicDescriptor {
    public var size:Float;

    // Negative if not applicable, otherwise [0,360) degrees
    public var angle:Float;

    // Negative if not applicable
    public var response:Float;

    // Negative if not applicable
    public var octave:Int;

    public function new() {
        size = angle = response = octave = -1;
    }
    public function setfrom(k:KeyPoint) {
        this.size     = k.size;
        this.angle    = k.angle;
        this.response = k.response;
        this.octave   = k.octave;
        return this;
    }
}

// Combined cv.feature2d.KeyPoint and feature description
class Feature {
    public var x:Float;
    public var y:Float;

    // Estimated velocity
    public var vx:Null<Float> = null;
    public var vy:Null<Float> = null;

    // Can have multiple descriptions per-position
    // pre: basics.length = complex.length
    public var basics:Array<BasicDescriptor>;
    public var complex:Array<ComplexDescriptor>;

    public function new() {}
    public function set(x:Float, y:Float) {
        this.x = x;
        this.y = y;
        basics = [];
        complex = [];
        return this;
    }
    public function setfrom(k:{public var x(get,never):Float; public var y(get,never):Float;}) {
        this.x = k.x;
        this.y = k.y;
        basics = [];
        complex = [];
        return this;
    }

    public function weakCopy() {
        var f = new Feature();
        f.basics = basics;
        f.x = x;
        f.y = y;
        f.vx = vx;
        f.vy = vy;
        return f;
    }

    public static function merge(out:Array<Feature>, count:Int):Int {
        Triangulate.sort(out, count, Triangulate.lex);
        var i = count-1;
        while (--i >= 0) {
            var p = out[i];
            var q = out[i+1];
            if (p.x != q.x || p.y != q.y)
                continue;

            p.basics = p.basics.concat(q.basics);
            out[i+1] = out[count-1];
            out[--count] = q;
        }
        return count;
    }
}

// General feature detector
interface FeatureDetector {
    // store in out, number of corners found in return value.
    public function detect(src:Mat, out:Array<Feature>):Int;
}

// Custom detector
class CustomDetector implements FeatureDetector {
    public var features:Array<Feature>;
    public function new(features) {
        this.features = features;
    }
    public function detect(_, out:Array<Feature>):Int {
        for (i in 0...features.length) {
            out[i] = features[i];
        }
        return features.length;
    }
}

// Masked feature detector
// Manual masking to work on 'any' feature detector.
class MaskedDetector implements FeatureDetector {
    public var mask:Array<Array<Vec2>>;
    public var detector:FeatureDetector;
    public function new(detector:FeatureDetector) {
        this.detector = detector;
        mask = [];
    }
    public function copy(ms:Array<Array<Vec2>>) {
        mask = [];
        for (m in ms) {
            var n:Array<Vec2> = [];
            for (p in m) n.push([p.x,p.y]);
            mask.push(n);
        }
    }
    function containsPoint(mask:Array<Vec2>, x:Vec2) {
        var ret = false;
        for (i in 0...mask.length) {
            var p = mask[i];
            var q = if (i == 0) mask[mask.length-1] else mask[i-1];
            if ((p.y < x.y && q.y >= x.y
              || q.y < x.y && p.y >= x.y)
              && (p.x <= x.x || q.x <= x.x)) {
                if ((p.x + (x.y - p.y) / (q.y - p.y) * (q.x - p.x)) < x.x)
                    ret = !ret;
            }
        }
        return ret;
    }
    public function detect(src:Mat, out:Array<Feature>):Int {
        var count = detector.detect(src, out);
        var i = 0;
        while (i < count) {
            var p = out[i];
            var found = false;
            for (m in mask) {
                if (containsPoint(m, p)) {
                    found = true;
                    break;
                }
            }
            if (found) i++;
            else {
                out[i] = out[--count];
                out[count] = p;
            }
        }
        return count;
    }
}

// Concatenated detector
class ConcatDetector implements FeatureDetector {
    public var detectors:Array<FeatureDetector>;
    public var buffer:Array<Feature>;
    public function new(?detectors:Null<Array<FeatureDetector>>) {
        this.detectors = if (detectors == null) [] else detectors;
        buffer = [];
    }
    public function detect(src:Mat, out:Array<Feature>):Int {
        var i = 0;
        for (d in detectors) {
            var cnt = d.detect(src, buffer);
            for (j in 0...d.detect(src, buffer)) out[i++] = buffer[j];
        }
        return Feature.merge(out, i);
    }
}

// 'Good Features To Track' detector
class GoodFeaturesDetector implements FeatureDetector {
    public var gfft:GFFT;
    var keypoints:Array<KeyPoint>;
    public function new(gfft:GFFT) {
        this.gfft = gfft;
        keypoints = [];
    }
    public function detect(img:Mat, out:Array<Feature>):Int {
        var count = gfft.detect(img, null, keypoints);
        for (i in 0...count) {
            var p = out[i];
            if (p == null) p = out[i] = new Feature();
            p.setfrom(keypoints[i].pt);
            p.basics.push(new BasicDescriptor().setfrom(keypoints[i]));
        }
        return count;
    }
}

// BRISK detector
class BriskDetector implements FeatureDetector {
    public var brisk:BRISK;
    var keypoints:Array<KeyPoint>;
    public function new(brisk:BRISK) {
        this.brisk = brisk;
        keypoints = [];
    }
    public function detect(img:Mat, out:Array<Feature>):Int {
        var count = brisk.detect(img, null, keypoints, null, false);
        for (i in 0...count) {
            var p = out[i];
            if (p == null) p = out[i] = new Feature();
            p.setfrom(keypoints[i].pt);
            p.basics.push(new BasicDescriptor().setfrom(keypoints[i]));
        }
        return count;
    }
}

// SURF detector
class SurfDetector implements FeatureDetector {
    public var surf:SURF;
    var keypoints:Array<KeyPoint>;
    public function new(surf:SURF) {
        this.surf = surf;
        keypoints = [];
    }
    public function detect(img:Mat, out:Array<Feature>):Int {
        var count = surf.detect(img, null, keypoints, null, false);
        for (i in 0...count) {
            var p = out[i];
            if (p == null) p = out[i] = new Feature();
            p.setfrom(keypoints[i].pt);
            p.basics.push(new BasicDescriptor().setfrom(keypoints[i]));
        }
        return count;
    }
}

// SIFT detector
class SiftDetector implements FeatureDetector {
    public var sift:SIFT;
    var keypoints:Array<KeyPoint>;
    public function new(sift:SIFT) {
        this.sift = sift;
        keypoints = [];
    }
    public function detect(img:Mat, out:Array<Feature>):Int {
        var count = sift.detect(img, null, keypoints, null, false);
        for (i in 0...count) {
            var p = out[i];
            if (p == null) p = out[i] = new Feature();
            p.setfrom(keypoints[i].pt);
            p.basics.push(new BasicDescriptor().setfrom(keypoints[i]));
        }
        // SIFT can return same feature multiple times
        // (different directions)
        // So we want to combine into single features.
        return Feature.merge(out, count);
    }
}

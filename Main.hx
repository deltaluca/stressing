package;

import ogl.GL;
import ogl.GLM;
import ogl.GLArray;
import glfw3.GLFW;
import glgui.*;
import glgui.GLFWEventLoop;
import #if cpp cpp #else neko #end.vm.Thread;
import #if cpp cpp #else neko #end.Lib;
import goodies.*;
import gl3font.*;
import systools.Dialogs;
import cv.core.Mat;

import Triangulate;
import Fonts;
import ImageProcessor;
import FeatureDetector;
import FeatureDescriptor;
import TrackingPane;

using glgui.Transform;
using glgui.Colour;

enum SMessage {
    SSetVideo     (v:Video);
    SSetProcessor (p:ImageProcessor);
    SSetDetector  (d:FeatureDetector);
    SSetFeatures  (fs:Array<Feature>, cnt:Int);
    SSetTracking  (d:TrackingInfo);

    SFrame(frame:Mat);
    SQueryFrame (ret:Thread, processed:Bool);

    SFeatures(features:Array<Feature>, count:Int);
    SQueryFeatures(ret:Thread);

    SMap(fs:Array<PV>, ts:Array<Int>);
    SQueryMap(ret:Thread);

    SReset;
    SStartTrack;
    SHalt;
    SAnimate;
    SLog(log:String);
    SCompute;
    SDump;

    SCallback(t:Thread);
    SData(w:Null<TWindow>, t:Dynamic);
    SClosedDisplay(w:TWindow);
}

class Main {
    static function main() {
        cv.Core.init();
        GLFW.setErrorCallback(function (x, y) throw ([x,y]:Array<Dynamic>));
        GLFW.init();

        GLFW.windowHint(GLFW.VISIBLE, 0);
        GLFW.windowHint(GLFW.RESIZABLE, 0);

        var eventLoop = new GLFWEventLoop();

        // Main window
        GLFWEventLoop.track('@ \033[33m$$Main::main\033[m OpenWindow -> GLFWEventLoop');
        Thread.current().sendMessage(TOpenWindow(Thread.current(), mainWindow));

        eventLoop.run();
        GLFW.terminate();
    }

    // Main window settings.
    static inline var windowWidth  = 550;
    static inline var windowHeight = 400;
    static inline var windowTitle  = "Stress";

    // Main TabMenu settings.
    static inline var tabMenuWidth     = 75;
    static inline var tabMenuTabHeight = 50;

    // ---------------------------------

    static var cVideoFile:String = null;
    static var cVideo:Video = null;
    static var cProcessor :ImageProcessor      = null;
    static var cDetector  :FeatureDetector     = null;
    static var cDescriptor:DescriptorExtractor = null;

    public static var displays:Array<{
        win:TWindow,
        onClose:Void->Void
    }> = [];
    public static function addDisplay(x:TWindow, ?onClose:Void->Void) {
        displays.push({win:x,onClose:onClose});
        if (cVideoFile != null) {
            GLFWEventLoop.track('@ \033[33mMain\033[m::addDisplay SetVideo -> [display]');
            x.thread.sendMessage(TOther(SSetVideo(cVideo)));
        }
    }
    public static function removeDisplay(x:TWindow) {
        for (i in 0...displays.length) {
            if (displays[i].win == x) {
                if (displays[i].onClose != null)
                    displays[i].onClose();
                displays[i] = displays[displays.length-1];
                displays.pop();
                break;
            }
        }
    }

    public static var window:TWindow;
    static function mainWindow() {
    var m:TMessage = Thread.readMessage(true);
    switch (m) {
    case TInit(win):
        window = win;
        GLFWEventLoop.track('@ \033[33mMain\033[m SetSize -> GLFWEventLoop');
        win.main.sendMessage(TSetSize (win, windowWidth, windowHeight));
        GLFWEventLoop.track('@ \033[33mMain\033[m SetTitle -> GLFWEventLoop');
        win.main.sendMessage(TSetTitle(win, windowTitle));
        GLFWEventLoop.track('@ \033[33mMain\033[m MakeVisible -> GLFWEventLoop');
        win.main.sendMessage(TMakeVisible(win));

        GLFW.makeContextCurrent(win.window);
        GL.init();

        GL.enable(GL.BLEND);
        GL.blendFunc(GL.SRC_ALPHA, GL.ONE_MINUS_SRC_ALPHA);

        GL.disable(GL.SCISSOR_TEST);

        GL.viewport(0, 0, windowWidth, windowHeight);

        // --------------------------------------------

        // Work Horse
        GLFWEventLoop.track('@ \033[33mMain\033[m $$current -> WorkHorse');
        Thread.create(WorkHorse.main)
        .sendMessage(Thread.current());

        // Display window
        var display:TWindow = null;
        GLFWEventLoop.track('@ \033[33mMain\033[m OpenWindow -> GLFWEventLoop');
        win.main.sendMessage(TOpenWindow(Thread.current(), Display.displayWindow));
        GLFWEventLoop.waitEvent('\033[33mMain\033[m', function (m:TMessage) {
            switch (m) {
            case TOpened(win):
                display = win;
                return true;
            default:
                return false;
            }
        });
        GLFWEventLoop.track('@ \033[33mMain\033[m Continue -> GLFWEventLoop');
        win.main.sendMessage(TContinue(win));
        addDisplay(display);

        // --------------------------------------------

        var gui = new Gui();
        var glfw = new GLFWGui(win.window);

        var background = Image.fromPNG("background01.png")
            .fit([0,0,windowWidth,windowHeight])
            .commit();

        var tabMenu = new TabMenu(
                tabMenuWidth, windowHeight,
                tabMenuTabHeight, "File"
            ).tab("File", 30)
             .tab("Controls")
             .tab("Processor")
             .tab("Detector")
             .tab("Descriptor")
             .tab("Tracking");

        var w = windowWidth - tabMenuWidth;
        var h = windowHeight;
        var controlsPane:ControlsPane = null;
        var processorPane:ConfigPane<ImageProcessor>  = null;
        var detectorPane :ConfigPane<FeatureDetector> = null;
        var descriptorPane:DescriptorPane = null;
        var trackingPane:TrackingPane = null;
        function refreshProcessor() {
            var processor = processorPane.graph.build();
            if (processor != null) {
                cProcessor = processor.extract();
                GLFWEventLoop.track('@ \033[33mMain\033[m SetProcessor -> WorkHorse');
                WorkHorse.self.sendMessage(TOther(SSetProcessor(cProcessor)));
            }
        }
        function refreshDetector() {
            var detector = detectorPane.graph.build();
            if (detector != null) {
                cDetector = detector.extract();
                GLFWEventLoop.track('@ \033[33mMain\033[m SetDetector -> WorkHorse');
                WorkHorse.self.sendMessage(TOther(SSetDetector(cDetector)));
            }
        }
        function refreshDescriptor() {
            var descriptor = descriptorPane.build();
            if (descriptor != null) {
                cDescriptor = descriptor.extract();
            }
        }
        function refreshTracking() {
            var info = trackingPane.info();
            GLFWEventLoop.track('@ \033[33mMain\033[m SetTracking -> WorkHorse');
            WorkHorse.self.sendMessage(TOther(SSetTracking(info)));
        }

        function loadVideo(f:String) {
            switch (f) {
            case "0":
                cVideoFile = "0";
                cVideo = new DistortVideo();
                GLFWEventLoop.track('@ \033[33mMain\033[m SetVideo -> WorkHorse');
                WorkHorse.self.sendMessage(TOther(SSetVideo(cVideo)));
                for (display in displays) {
                    GLFWEventLoop.track('@ \033[33mMain\033[m SetVideo -> [display]');
                    display.win.thread.sendMessage(TOther(SSetVideo(cVideo)));
                }
            default:
                if (!sys.FileSystem.exists(f))
                    Dialogs.message("File does not exist", "File does not exist", true);
                else if (sys.FileSystem.isDirectory(f))
                    Dialogs.message("File is a directory", "File is a directory", true);
                else {
                    cVideoFile = f;
                    cVideo = new CVVideo(f);
                    GLFWEventLoop.track('@ \033[33mMain\033[m SetVideo -> WorkHorse');
                    WorkHorse.self.sendMessage(TOther(SSetVideo(cVideo)));
                    for (display in displays) {
                        GLFWEventLoop.track('@ \033[33mMain\033[m SetVideo -> [display]');
                        display.win.thread.sendMessage(TOther(SSetVideo(cVideo)));
                    }
                }
            }
        }
        function loadConfig(f:String) {
            var json : {
                processor  : Dynamic,
                detector   : Dynamic,
                descriptor : Dynamic,
                tracking   : Dynamic
            } = haxe.Json.parse(sys.io.File.getContent(f));
            processorPane.graph.clear();
            ProcessorPane.parse(processorPane, cast json.processor);
            processorPane.graph.format();

            detectorPane.graph.clear();
            DetectorPane.parse(detectorPane, cast json.detector);
            detectorPane.graph.format();

            descriptorPane.parse(cast json.descriptor);
            trackingPane.parse(cast json.tracking);

            refreshProcessor();
            refreshDetector();
            refreshDescriptor();
            refreshTracking();
        }

        var menuPanes:Map<String,{var group(default,null):Group;}> = [
            "File" => {
                var pane = new FilePane();
                pane.onLoadFile = loadVideo;
                pane.onSaveConfig = function (f) {
                    var data = haxe.Json.stringify({
                        processor  : processorPane .graph.write(),
                        detector   : detectorPane  .graph.write(),
                        descriptor : descriptorPane.write(),
                        tracking   : trackingPane.info()
                    });

                    var out = sys.io.File.write(f, false);
                    out.writeString(data);
                    out.flush();
                    out.close();
                }
                pane.onLoadConfig = loadConfig;
                pane;
            },
            "Controls" => {
                var pane = controlsPane = ControlsPane.create(w, h);
                pane.onReset = function() {
                    GLFWEventLoop.track('@ \033[33mMain\033[m Reset -> WorkHorse');
                    WorkHorse.self.sendMessage(TOther(SReset));
                    refreshProcessor();
                    refreshDetector();
                    refreshDescriptor();
                    refreshTracking();
                    GLFWEventLoop.track('@ \033[33mMain\033[m SetVideo -> WorkHorse');
                    WorkHorse.self.sendMessage(TOther(SSetVideo(cVideo)));
                };
                pane.onTrack = function() {
                    GLFWEventLoop.track('@ \033[33mMain\033[m StartTrack -> WorkHorse');
                    WorkHorse.self.sendMessage(TOther(SStartTrack));
                };
                pane.onHalt = function() {
                    GLFWEventLoop.track('@ \033[33mMain\033[m Halt -> WorkHorse');
                    WorkHorse.self.sendMessage(TOther(SHalt));
                };
                pane.onCompute = function () {
                    GLFWEventLoop.track('@ \033[33mMain\033[m Compute -> WorkHorse');
                    WorkHorse.self.sendMessage(TOther(SCompute));
                }
                pane.onAnimate = function() {
                    GLFWEventLoop.track('@ \033[33mMain\033[m Animate -> WorkHorse');
                    WorkHorse.self.sendMessage(TOther(SAnimate));
                };
                pane.onDump = function () {
                    WorkHorse.self.sendMessage(TOther(SDump));
                };
                pane;
            },
            "Processor" => {
                var pane = processorPane = ProcessorPane.create(w, h);
                pane.group.element(ConfigPane.genRefresh(refreshProcessor)
                    .position([-tabMenuWidth/2,windowHeight-tabMenuWidth/2]));
                pane;
            },
            "Detector" => {
                var pane = detectorPane = DetectorPane.create(w, h);
                pane.group.element(ConfigPane.genRefresh(refreshDetector)
                    .position([-tabMenuWidth/2,windowHeight-tabMenuWidth/2]));
                pane;
            },
            "Descriptor" => {
                var pane = descriptorPane = DescriptorPane.create(w, h);
                pane.group.element(ConfigPane.genRefresh(refreshDescriptor)
                    .position([-tabMenuWidth/2,windowHeight-tabMenuWidth/2]));
                pane;
            },
            "Tracking" => {
                var pane = trackingPane = TrackingPane.create(w, h);
                pane.group.element(ConfigPane.genRefresh(refreshTracking)
                    .position([-tabMenuWidth/2,windowHeight-tabMenuWidth/2]));
                pane;
            }
        ];
        for (pane in menuPanes) pane.group.position([tabMenuWidth, 0]);

        // --------------------------------------------

        var args = Sys.args();
        var initConfig = null;
        for (a in args) {
            if (a.substr(0,7)=="-video=")
                loadVideo(a.substr(7));
            if (a.substr(0,8)=="-config=")
                loadConfig(a.substr(8));
        }

        // --------------------------------------------

        while (true) {
        var msg:TMessage = Thread.readMessage(true);
        GLFWEventLoop.track('\033[33mMain\033[m ${msg}');
        switch (msg) {
        case TOther(x): var y:SMessage = x; switch (y) {
        case SClosedDisplay(win):
            removeDisplay(win);
        case SLog(log):
            controlsPane.log(log);
        default:
        }
        case TUpdate(shouldClose):
            if (shouldClose)
                if (Dialogs.confirm("", "Are you sure you want to exit?", false)) break;
                else {
                    GLFWEventLoop.track('@ \033[33mMain\033[m NotClosing -> GLFWEventLoop');
                    win.main.sendMessage(TNotClosing(win));
                }

            // ----------------------------------------
            GL.clear(GL.COLOR_BUFFER_BIT);
            glfw.updateState(gui);

            gui.render(background);
            var pane = menuPanes[tabMenu.currentTab];
            if (pane != null)
                gui.render(pane.group);
            gui.render(tabMenu.group);

            gui.flush();
            GLFW.swapBuffers(win.window);

            // ----------------------------------------
        GLFWEventLoop.track('@ \033[33mMain\033[m Continue -> GLFWEventLoop');
        win.main.sendMessage(TContinue(win)); default: }
        }

        // --------------------------------------------

        gui.destroy();


        GLFWEventLoop.track('@ \033[33mMain\033[m CloseWindow -> GLFWEventLoop');
        win.main.sendMessage(TCloseWindow(win));
        GLFWEventLoop.track('@ \033[33mMain\033[m Terminate -> GLFWEventLoop');
        win.main.sendMessage(TTerminate);

        // --------------------------------------------

        GLFWEventLoop.track('@ \033[33mMain\033[m Terminate -> WorkHorse');
        WorkHorse.self.sendMessage(TTerminate);
        GLFWEventLoop.waitEvent('\033[33mMain\033[m', function (msg:TMessage) {
            return Type.enumEq(msg, TTerminate);
        });

        GLFWEventLoop.track('%\033[33mMain\033[m TERMINATED');
    default: throw "Main nooooooo"; }}
}










/*
enum Msg {
    MsgShouldClose;
    MsgHasClosed;
    MsgBoxEditor(box:Dynamic); //Box<T>

    MsgOpen(file:String);
    MsgProcessor(processor:ImageProcessor);
    MsgDetector (detector :FeatureDetector);
}

class Tabbed {
    public var ctab(default,null):String;
    var tabmap:Map<String, PanelButton>;
    var elements:Array<Dynamic>;
    var panes:Map<String, Group>;

    public function new(ctab:String) {
        this.ctab = ctab;
        tabmap = new Map<String, PanelButton>();
        panes = new Map<String, Group>();
        elements = [new Panel()
            .colour([0.5,0.5,0.5,1.0])
            .radius(0)
            .fit([width-1,0,2,400])
        .commit()];
    }

    public static inline var width  = 75;
    public static inline var height = 50;

    var cnt = 0;
    public function pushTab(name:String, fontSize=-1, pane:Group=null) {
        panes[name] = pane;
        tabmap[name] = new PanelButton(true)
            .font(Fonts.dejavu)
            .size(fontSize)
            .text(GLString.make(name, [1,1,1,1]))
            .disabledText(GLString.make(name, [0.4,0.4,0.4,1]))

            .fit([0,height*cnt,width,height])
            .radius(0)
            .thickness(5)

            .colour            ([0.1,0.1,0.1,0.75])
            .borderColour      ([0.1,0.1,0.1,0.75])
            .pressColour       ([0.1,0.1,0.1,0.75])
            .overColour        ([1.0,1.0,1.0,0.15])

            .toggled (name == ctab)
            .disabled(name == ctab)

            .press(function (x) if (x) {
                tabmap[ctab].toggled(false).disabled(false);
                ctab = name;
                tabmap[name].disabled(true);
            })
        .commit();

        elements.unshift(tabmap[name]);
        elements.push(new Panel()
            .colour([0.5,0.5,0.5,0.5])
            .radius(0)
            .fit([0,(cnt+1)*height-1,width,2])
        .commit());

        cnt++;
        return this;
    }

    public function render(gui:Gui) {
        var pane = panes[ctab];
        if (pane != null) gui.render(pane);
        for (e in elements) gui.render(e);
    }
}

class Box<T> {
    static var nextId = 0;
    public var id:Int;
    public var graph:ConfigGraph<T>;

    public var forth:Box<T>;
    public var side:Maybe<Box<T>>;

    public var backPos:Vec2;
    public var forthPos:Vec2;
    public var sidePos:Vec2;

    public var build:Void->T;

    public var display:Thread = null;

    public var group:Group;
    public function new(graph:ConfigGraph<T>, width:Int, height:Int, title:String, noBar=false, barf=25.0, root=false, side=false) {
        id = nextId++;

        var titleText;
        var drag = false;
        var offset:Vec2 = [0,0];
        var base:Vec2 = [0,0];
        var t0, t1, t2;

        var self = this;
        group = new Group();

        this.graph = graph;
        graph.add(this);

        function mid(b:Vec4):Vec2 {
            return [b.x + b.z/2, b.y + b.w/2];
        }

        if (root) {
            // bottom joint
            group.element(t1 = new Panel()
                .fit([width/2-10,width-10,20,20])
                .radius(10)
                .colour([1,1,1,1])
                .commit()
            )
            .element(new Panel()
                .fit([width/2-9,width-9,18,18])
                .radius(9)
                .colour([0,1,0,1])
                .commit()
            )
            .element(new Mouse()
                .interior(t1.internal)
                .enter(function () graph.enter(self, LinkForth))
                .exit (function () graph.exit (self, LinkForth))
                .press(function (_,but) {
                    if (!Type.enumEq(but, MouseLeft)) return;
                    graph.startLink(self, LinkForth);
                })
                .release(function (_,but,_) {
                    if (!Type.enumEq(but, MouseLeft)) return;
                    graph.endLink();
                })
                .commit()
            )

            // body
            .element(t0 = new Panel()
                .fit([0,0,width,width])
                .colour([0,0,0,1])
                .radius(width/2)
                .commit())
            .element(new Panel()
                .fit([2,2,width-4,width-4])
                .colour([0.9,0.9,0.9,1])
                .radius(width/2-2)
                .commit())

            // drag
            .element(new Mouse()
                .interior(t0.internal)
                .press(function (_, but) {
                    graph.bringToFront(self);
                    if (!Type.enumEq(but, MouseLeft)) return;
                    var mpos:Vec2 = GLFW.getCursorPos(Mains.window);
                    offset = mpos;
                    drag = true;
                })
                .release(function (_, but, _) {
                    if (!Type.enumEq(but, MouseLeft)) return;
                    drag = false;
                })
                .mouse(function (_) {
                    if (!drag) return;
                    var mpos:Vec2 = GLFW.getCursorPos(Mains.window);
                    var pos = group.getPosition() + (mpos - offset);
                    offset = mpos;
                    group.position(pos);
                })
            );
            forthPos = mid(t1.bounds().extract());
            return;
        }

        // top joint
        group.element(t0 = new Panel()
            .fit([width/2-10,-10,20,20])
            .radius(10)
            .colour([1,1,1,1])
            .commit()
        )
        .element(new Panel()
            .fit([width/2-9,-9,18,18])
            .radius(9)
            .colour([1,0,0,1])
            .commit()
        )
        .element(new Mouse()
            .interior(t0.internal)
            .enter(function () graph.enter(self, LinkBack))
            .exit (function () graph.exit (self, LinkBack))
            .press(function (_,but) {
                if (!Type.enumEq(but, MouseLeft)) return;
                graph.startLink(self, LinkBack);
            })
            .release(function (_,but,_) {
                if (!Type.enumEq(but, MouseLeft)) return;
                graph.endLink();
            })
            .commit()
        )

        // bottom joint
        .element(t1 = new Panel()
            .fit([width/2-10,height-10,20,20])
            .radius(10)
            .colour([1,1,1,1])
            .commit()
        )
        .element(new Panel()
            .fit([width/2-9,height-9,18,18])
            .radius(9)
            .colour([0,1,0,1])
            .commit()
        )
        .element(new Mouse()
            .interior(t1.internal)
            .enter(function () graph.enter(self, LinkForth))
            .exit (function () graph.exit (self, LinkForth))
            .press(function (_,but) {
                if (!Type.enumEq(but, MouseLeft)) return;
                graph.startLink(self, LinkForth);
            })
            .release(function (_,but,_) {
                if (!Type.enumEq(but, MouseLeft)) return;
                graph.endLink();
            })
            .commit()
        );

        if (side) {
            // Side joint
            group.element(t2 = new Panel()
                .fit([width-10,height/2-10,20,20])
                .radius(10)
                .colour([1,1,1,1])
                .commit()
            )
            .element(new Panel()
                .fit([width-9,height/2-9,18,18])
                .radius(9)
                .colour([0,0,1,1])
                .commit()
            )
            .element(new Mouse()
                .interior(t2.internal)
                .enter(function () graph.enter(self, LinkSide))
                .exit (function () graph.exit (self, LinkSide))
                .press(function (_,but) {
                    if (!Type.enumEq(but, MouseLeft)) return;
                    graph.startLink(self, LinkSide);
                })
                .release(function (_,but,_) {
                    if (!Type.enumEq(but, MouseLeft)) return;
                    graph.endLink();
                })
                .commit()
            );
        }

        // box panels
        group.element(new Panel()
            .fit([0,0,width,height])
            .colour([0,0,0,1])
            .radius(20)
            .commit())
        .element(new Panel()
            .fit([2,2,width-4,height-4])
            .colour([0.9,0.9,0.9,1])
            .radius(17)
            .commit());
        if (!noBar) {
            group.element(new Panel()
                .fit([2,barf,width-4,2])
                .colour([0.5,0.5,0.5,1])
                .radius(0)
                .commit());
        }

        // title
        group.element(titleText = new Text()
            .fit([24,2,width-48,noBar ? height - 6 : barf-1])
            .text(GLString.make(title, [0,0,0,1]))
            .font(Fonts.dejavu)
            .halign(TextAlignCentre)
            .valign(TextAlignCentre)
            .commit())
        .element(new Mouse()
            .fit(titleText.bounds().extract())
            .press(function (_, but) {
                graph.bringToFront(self);
                if (!Type.enumEq(but, MouseLeft)) return;
                var mpos:Vec2 = GLFW.getCursorPos(Mains.window);
                offset = mpos;
                drag = true;
            })
            .release(function (_, but, _) {
                if (!Type.enumEq(but, MouseLeft)) return;
                drag = false;
            })
            .mouse(function (_) {
                if (!drag) return;
                var mpos:Vec2 = GLFW.getCursorPos(Mains.window);
                var pos = group.getPosition() + (mpos - offset);
                offset = mpos;
                group.position(pos);
            })
        )

        // close/minimise buttons
        .element(new PanelButton()
            .fit([6,9,14,14])
            .thickness(0)
            .colour([1,0,0,1])
            .radius(7)
            .text(GLString.make("X", [1,1,1,1]))
            .size(16)
            .font(Fonts.dejavu)
            .press(function (_) {
                graph.remove(self);
            })
            .commit())
        .element(new PanelButton()
            .fit([width-6-14,9,14,14])
            .thickness(0)
            .colour([0,0,0,1])
            .radius(7)
            .text(GLString.make("袈", [1,1,1,1]))
            .size(16)
            .font(Fonts.dejavu)
            .commit());

        backPos  = mid(t0.bounds().extract());
        forthPos = mid(t1.bounds().extract());
        if (side)
            sidePos = mid(t2.bounds().extract());
    }

    function xform(x:Vec2):Vec2 {
        var p = group.getPosition();
        return Mat3x2.translate(p.x, p.y) * x;
    }
    public function backPosition () return xform(backPos);
    public function forthPosition() return xform(forthPos);
    public function sidePosition () return xform(sidePos);

    public function destroy() {
        group.destroy();
    }
    public function render(gui:Gui) {
        gui.render(group);
    }
}

enum ConfigLink {
    LinkForth;
    LinkBack;
    LinkSide;
}

class ConfigGraph<T> {
    public var root (default,null):Box<T>;
    public var boxes(default,null):Array<Box<T>>;

    public var group:Group;

    var link:Maybe<ConfigLink> = null;
    var clink:Maybe<Box<T>>;
    var overs:Array<{box:Box<T>,link:ConfigLink}>;

    public function new() {
        root = null;
        boxes = [];
        overs = [];
        group = new Group()
        .element(new Custom()
        .apply(function (gui, mpos, proj, xform) {
            var drawing = gui.drawings();
            drawing.setTransform(proj * xform);
            drawing.swapFills();
            for (box in boxes) {
                if (box.forth != null) {
                    var p:Vec2 = box.forthPosition();
                    var q:Vec2 = box.forth.backPosition();
                    var d:Vec2 = [p.y-q.y, q.x-p.x];
                    d *= 2/Math.sqrt(d.x*d.x+d.y*d.y);
                    drawing.pushVertex(p+d, [0.5,1,0.5,1]);
                    drawing.pushVertex(p-d, [0.5,1,0.5,1]);
                    drawing.pushVertex(q-d, [1,0.5,0.5,1]);

                    drawing.pushVertex(p+d, [0.5,1,0.5,1]);
                    drawing.pushVertex(q-d, [1,0.5,0.5,1]);
                    drawing.pushVertex(q+d, [1,0.5,0.5,1]);
                }
                if (box.side != null) {
                    var p:Vec2 = box.sidePosition();
                    var q:Vec2 = box.side.extract().backPosition();
                    var d:Vec2 = [p.y-q.y, q.x-p.x];
                    d *= 2/Math.sqrt(d.x*d.x+d.y*d.y);
                    drawing.pushVertex(p+d, [0.5,0.5,1,1]);
                    drawing.pushVertex(p-d, [0.5,0.5,1,1]);
                    drawing.pushVertex(q-d, [1,0.5,0.5,1]);

                    drawing.pushVertex(p+d, [0.5,0.5,1,1]);
                    drawing.pushVertex(q-d, [1,0.5,0.5,1]);
                    drawing.pushVertex(q+d, [1,0.5,0.5,1]);
                }
            }

            if (clink != null && mpos != null) {
                var clink = clink.extract();
                drawing.drawLine(
                    switch (link.extract()) {
                    case LinkForth: clink.forthPosition();
                    case LinkBack : clink.backPosition();
                    case LinkSide : clink.sidePosition();
                    }, mpos.extract(),
                       [1,1,1,0.75]
                );
            }
        }));

        // Root element.
        root = new Box(this, 30, 0, "", false, 0, true);
    }

    public function bringToFront(x:Box<T>) {
        group.bringToFront(x.group);
    }

    public function add(x:Box<T>) {
        boxes.push(x);
        group.element(x.group);
    }
    public function remove(x:Box<T>) {
        boxes.remove(x);
        group.removeElement(x.group);
        for (b in boxes)
            if (b.forth == x) b.forth = null;
    }

    public function startLink(x:Box<T>, link:ConfigLink) {
        clink = x;
        this.link = link;
    }
    public function endLink() {
        var link = link.extract();
        var clink:Box<T> = clink.extract();
        if (overs.length == 0) {
            if (Type.enumEq(link, LinkForth))
                clink.forth = null;
            else if (Type.enumEq(link, LinkSide))
                clink.side = null;
            this.clink = null;
            this.link = null;
            return;
        }
        var olink = overs.pop();
        if (Type.enumEq(link, LinkForth))
            clink.forth = olink.box;
        else if (Type.enumEq(link, LinkBack)) {
            if (Type.enumEq(olink.link, LinkForth))
                 olink.box.forth = clink;
            else olink.box.side  = clink;
        } else if (Type.enumEq(link, LinkSide))
            clink.side = olink.box;

        this.clink = null;
        this.link = null;
    }

    function compatibleLink(from:ConfigLink, to:ConfigLink) {
        return switch (from) {
        case LinkForth: Type.enumEq(to, LinkBack);
        case LinkSide:  Type.enumEq(to, LinkBack);
        case LinkBack:  !Type.enumEq(to, LinkBack);
        }
    }
    public function enter(x:Box<T>, link:ConfigLink) {
        if (this.link != null && compatibleLink(this.link.extract(), link) && x != clink.extract())
            overs.push({box:x, link:link});
    }
    public function exit(x:Box<T>, link:ConfigLink) {
        if (this.link != null && compatibleLink(this.link.extract(), link) && x != clink.extract()) {
            var i = 0;
            while (i < overs.length) {
                if (overs[i].box == x) {
                    overs[i] = overs[overs.length - 1];
                    overs.pop();
                    break;
                }
            }
        }
    }

    public var multi:Array<T>->T;

    public function build(root:Box<T>=null):Maybe<T> {
        try {
            var visited = new Map<Int,Bool>();
            var results = [];
            var cur:Box<T> =
                if (root != null) root;
                else this.root;
            while (cur != null) {
                if (visited.exists(cur.id)) throw "Infinite loop in configuration graph";
                visited[cur.id] = true;
                if (cur != this.root)
                    results.push(cur.build());
                cur = cur.forth;
            }
            return cast multi(results);
        }
        catch(e:Dynamic) {
            Dialogs.message("Error!", Std.string(e), true);
            return null;
        }
    }
}

class Mains {
    public static var window:Window;
    public static var display:Thread;

    static function closeDisplay(x:Thread) {
        x.sendMessage(MsgShouldClose);
        while (true) {
            var msg:Msg = Thread.readMessage(true);
            if (Type.enumEq(msg, MsgHasClosed))
                return;
        }
    }

    static function main() {
        GLFW.setErrorCallback(function (err, msg) {
            throw 'err=$err "$msg"';
        });
        GLFW.init();

        GLFW.windowHint(GLFW.RESIZABLE, 0);
        GLFW.windowHint(GLFW.SAMPLES, 16);
        window = GLFW.createWindow(550, 400, "Stress");
        GLFW.makeContextCurrent(window);
        GLFW.setWindowPos(window, 0, 0);

        var cache = new glgui.Cache();

        GL.init();
        GL.enable(GL.BLEND);
        GL.blendFunc(GL.SRC_ALPHA, GL.ONE_MINUS_SRC_ALPHA);
        GL.disable(GL.SCISSOR_TEST);

        var openDisplays:Array<Thread> = [];

        var dejavu = new Font("dejavu.sans.dat", "dejavu.sans.png");

        var gui = new Gui();
        var glfw = new GLFWGui(window);

        var bg = Image.fromPNG("background01.png")
            .fit([0,0,550,400])
            .commit();

        var currentVideo:Maybe<String> = null;
        var currentProcessor:Maybe<ImageProcessor> = null;

        var fileGroup = new Group();
        var cnt = 0;
        function fileDial(name:String, press:Void->Void) {
            var y = (cnt++)*40;
            fileGroup.element(new PanelButton(false)
                .font(dejavu)
                .thickness(3)
                .radius(15)
                .text(GLString.make(name, [0.8,0.8,0.8,1]))
                .size(15)
                .fit([90,290-36/2+y,100,36])
                .colour([0.1,0.1,0.1,1.0])
                .overColour([1.0,1.0,1.0,0.1])
                .press(function (_) press())
                .commit());
        }
        fileDial("Load Video",  function () {
            var filters: FILEFILTERS = {
                count: 1,
                descriptions: ["Video files"],
                extensions: ["*.avi;*.mpeg;*.m4v;*.mp4"]
            };
            var result = Dialogs.openFile("Select a Video file", "", filters);
            if (result != null) {
                display.sendMessage(MsgOpen(result[0]));
                currentVideo = result[0];
            }
        });
        fileDial("Load Config", function () {});
        fileDial("Save Config", function () {});


        function genModifier(name:String, space:Float) {
            var group = new Group();
            var text = new Text()
                .font(Fonts.dejavu)
                .text(GLString.make(name+":", [0.2,0.2,0.2,1.0]))
                .size(15)
                .halign(TextAlignRight)
                .valign(TextAlignTop)
                .position([-2,0])
                .commit();
            var fit = text.bounds().extract();
            var pan = new Panel()
                .fit([2,0,space,fit.w])
                .radius(5)
                .colour([1,1,1,1])
                .commit();
            var inp = new TextInput()
                .font(Fonts.dejavu)
                .size(15)
                .fit(pan.getFit() + new Vec4([4.5,3,-6,0]))
                .colour([0,0,0,1])
                .multiline(false)
                .commit();
            group.element(text)
                 .element(pan)
                 .element(inp);
            return { group: group, inp: inp };
        }

        function genCheckModifier(name:String) {
            var group = new Group();
            var text = new Text()
                .font(Fonts.dejavu)
                .text(GLString.make(name+":", [0.2,0.2,0.2,1.0]))
                .size(15)
                .halign(TextAlignRight)
                .valign(TextAlignTop)
                .position([-2,0])
                .commit();
            var check = new PanelButton(true)
                .fit([6,1,16,16])
                .radius(0)
                .size(15)
                .font(Fonts.dejavu)
                .text(GLString.make("X", [0,0,0,1]))
                .disabledText(GLString.make("X", [0.8,0.8,0.8,1]))
                .colour([1,1,1,1])
                .borderColour([0.7,0.7,0.7,1])
                .overColour([0,0,0,0.2])
                .pressColour([0.5,0.5,0.5,1])
                .commit();
            group.element(text)
                 .element(check);
            return { group: group, check: check };
        }

        var pgraph = new ConfigGraph();
        pgraph.multi = function (xs) {
            return new MultiProcessor(xs);
        };
        function genPMedian() {
            var box = new Box(pgraph, 140, 60, "Median Blur");
            var kernel = genModifier("Kernel Size", 13*3);
            kernel.group.position([140-45,33]);
            kernel.inp.maxChars(3)
                      .allowed(~/[0-9]/)
                      .text(GLString.make("5", [0,0,0,1]));
            box.group.element(kernel.group);
            box.build = function () {
                var kernel = Std.parseInt(kernel.inp.getText().toString());
                if (kernel % 2 != 1) throw "Median Blur : Kernel size must be an odd number";
                return new MedianProcessor(kernel);
            }
            return box;
        }
        function genPGaussian() {
            var box = new Box(pgraph, 140, 60, "Gaussian Blur");
            var kernel = genModifier("Kernel Size", 13*3);
            kernel.group.position([140-45,33]);
            kernel.inp.maxChars(3)
                      .allowed(~/[0-9]/)
                      .text(GLString.make("5", [0,0,0,1]));
            box.group.element(kernel.group);
            box.build = function () {
                var kernel = Std.parseInt(kernel.inp.getText().toString());
                if (kernel % 2 != 1) throw "Gaussian Blur : Kernel size must be an odd number";
                return new GaussianProcessor(kernel);
            }
            return box;
        }
        function genPBilateral() {
            var box = new Box(pgraph, 140, 96, "Bilateral Blur");

            var diameter = genModifier("Diameter", 13*3);
            diameter.group.position([125-45,33]);
            diameter.inp.maxChars(3)
                        .allowed(~/[0-9]/)
                        .text(GLString.make("5", [0,0,0,1]));
            box.group.element(diameter.group);

            var colour = genModifier("σ-colour", 13*4);
            colour.group.position([125-45,33+18]);
            colour.inp.maxChars(9)
                      .allowed(~/[0-9.eE+\-]/)
                      .text(GLString.make("0.01", [0,0,0,1]));
            box.group.element(colour.group);

            var space = genModifier("σ-space", 13*4);
            space.group.position([125-45,33+18*2]);
            space.inp.maxChars(9)
                     .allowed(~/[0-9.eE+\-]/)
                     .text(GLString.make("0.01", [0,0,0,1]));
            box.group.element(space.group);

            box.build = function () {
                var diameter = Std.parseInt(diameter.inp.getText().toString());
                var colour = Std.parseFloat(colour.inp.getText().toString());
                var space = Std.parseFloat(space.inp.getText().toString());

                return new BilateralProcessor(diameter, colour, space);
            }

            return box;
        }
        function genPHistogram() {
            var box = new Box(pgraph, 140, 50, "Histogram\nEqualisation", true);
            box.build = function() return new HistogramProcessor();
            return box;
        }
        function genPAdaptiveHistogram() {
            var box = new Box(pgraph, 140, 110, "Adaptive\nHist. Eq.", false, 40);

            var nX = genModifier("num-x", 13*3);
            nX.group.position([120-48,48]);
            nX.inp.maxChars(3)
                  .allowed(~/[0-9]/)
                  .text(GLString.make("5", [0,0,0,1]));
            box.group.element(nX.group);

            var nY = genModifier("num-y", 13*3);
            nY.group.position([120-48,48+18]);
            nY.inp.maxChars(3)
                  .allowed(~/[0-9]/)
                  .text(GLString.make("5", [0,0,0,1]));
            box.group.element(nY.group);

            var limit = genModifier("limit", 13*4);
            limit.group.position([120-48,48+18*2]);
            limit.inp.maxChars(9)
                  .allowed(~/[0-9.eE+\-]/)
                  .text(GLString.make("0.05", [0,0,0,1]));
            box.group.element(limit.group);

            box.build = function() {
                var nX = Std.parseInt(nX.inp.getText().toString());
                var nY = Std.parseInt(nY.inp.getText().toString());
                var limit = Std.parseFloat(limit.inp.getText().toString());

                return new AdaptiveHistogramProcessor(nX, nY, limit);
            }

            return box;
        }
        function genPCustom() {
            var box = new Box(pgraph, 170,60, "Custom");
            var txt = new TextInput()
                .fileInput(true)
                .fit([4+50+8,30,106,22])
                .size(13)
                .colour([0,0,0,1])
                .text(GLString.make("<select>", [0,0,0,1]))
                .font(Fonts.dejavu)
                .commit();
            var but = new PanelButton()
                .fit([7,31,50,22])
                .colour([0.9,0.9,0.9,1.0])
                .borderColour([0.4,0.4,0.4,1.0])
                .pressColour([0.1,0.1,0.1,1.0])
                .size(13)
                .radius(6)
                .thickness(2)
                .text(GLString.make("Select", [0,0,0,1]))
                .font(Fonts.dejavu)
                .press(function (_) {
                    var file = Dialogs.openFile(
                        "Select File",
                        "Select file for custom processor",
                        {
                            count: 1,
                            descriptions: ["Haxe (hscript) file"],
                            extensions: ["*.hx"]
                        }
                    );
                    if (file != null) {
                        txt.text(GLString.make(file[0], [0,0,0,1]));
                        txt.gotoEnd();
                    }
                })
                .commit();
            box.group.element(txt);
            box.group.element(but);

            box.build = function () {
                var path = txt.getText().toString();
                if (!sys.FileSystem.exists(path))
                    throw '"$path" is not a valid file path';
                if (sys.FileSystem.isDirectory(path))
                    throw '"$path" is not a file';
                try {
                    return new CustomProcessor(
                        Config.loadCustom(path)
                    );
                }
                catch(e:Dynamic) {
                    throw 'hscript parse error: $e';
                }
            }

            return box;
        }

        var dgraph = new ConfigGraph();
        dgraph.multi = function (xs) {
            return new ConcatDetector(xs);
        };
        function genDBrisk() {
            var box = new Box(dgraph, 170, 100, "BRISK");

            var threshold = genModifier("Threshold", 13*3);
            threshold.group.position([150-40,33]);
            threshold.inp.maxChars(4)
                         .allowed(~/[0-9]/)
                         .text(GLString.make("30", [0,0,0,1]));
            box.group.element(threshold.group);

            var octaves = genModifier("Octaves", 13*3);
            octaves.group.position([150-40,33+18]);
            octaves.inp.maxChars(3)
                       .allowed(~/[0-9]/)
                       .text(GLString.make("3", [0,0,0,1]));
            box.group.element(octaves.group);

            var scale = genModifier("Pattern Scale", 13*4);
            scale.group.position([150-40,33+18*2]);
            scale.inp.maxChars(5)
                     .allowed(~/[0-9.eE+\-]/)
                     .text(GLString.make("1.0", [0,0,0,1]));
            box.group.element(scale.group);

            box.build = function () {
                var threshold = Std.parseInt(threshold.inp.getText().toString());
                var octaves = Std.parseInt(octaves.inp.getText().toString());
                var scale = Std.parseFloat(scale.inp.getText().toString());
                return new BriskDetector(cv.Features2D.brisk(threshold, octaves, scale));
            }

            return box;
        }
        function genDGFFT() {
            var box = new Box(dgraph, 180, 150, "GFFT");

            var features = genModifier("Max Features", 13*3);
            features.group.position([160-40,33]);
            features.inp.maxChars(4)
                        .allowed(~/[0-9]/)
                        .text(GLString.make("100", [0,0,0,1]));
            box.group.element(features.group);

            var quality = genModifier("Quality", 13*4);
            quality.group.position([160-40,33+18]);
            quality.inp.maxChars(5)
                        .allowed(~/[0-9.eE+\-]/)
                        .text(GLString.make("0.1", [0,0,0,1]));
            box.group.element(quality.group);

            var distance = genModifier("Min Distance", 13*4);
            distance.group.position([160-40,33+18*2]);
            distance.inp.maxChars(5)
                        .allowed(~/[0-9.eE+\-]/)
                        .text(GLString.make("5.0", [0,0,0,1]));
            box.group.element(distance.group);

            var block = genModifier("Block Size", 13*3);
            block.group.position([160-40,33+18*3]);
            block.inp.maxChars(5)
                     .allowed(~/[0-9]/)
                     .text(GLString.make("3", [0,0,0,1]));
            box.group.element(block.group);

            var harris = genCheckModifier("Harris Corners");
            harris.group.position([160-40,33+18*4]);
            harris.check.toggled(false);
            box.group.element(harris.group);

            var param = genModifier("Harris Param.", 13*4);
            param.group.position([160-40,33+18*5]);
            param.inp.maxChars(5)
                     .allowed(~/[0-9.eE+\-]/)
                     .text(GLString.make("0.04", [0,0,0,1]));
            box.group.element(param.group);

            box.build = function () {
                var features = Std.parseInt(features.inp.getText().toString());
                var quality = Std.parseFloat(quality.inp.getText().toString());
                var distance = Std.parseFloat(distance.inp.getText().toString());
                var block = Std.parseInt(block.inp.getText().toString());
                var harris = harris.check.getToggled();
                var param = Std.parseFloat(param.inp.getText().toString());
                return new GoodFeaturesDetector(cv.Features2D.gfft(
                    features, quality, distance, block,
                    harris, param
                ));
            }

            return box;
        }

        function genDSURF() {
            var box = new Box(dgraph, 190, 110, "SURF");

            var hthreshold = genModifier("Hessian T.hold", 13*4);
            hthreshold.group.position([170-40,33]);
            hthreshold.inp.maxChars(5)
                          .allowed(~/[0-9eE+\-]/)
                          .text(GLString.make("500", [0,0,0,1]));
            box.group.element(hthreshold.group);

            var octaves = genModifier("Octaves", 13*2);
            octaves.group.position([170-40,33+18]);
            octaves.inp.maxChars(2)
                      .allowed(~/[0-9]/)
                      .text(GLString.make("4", [0,0,0,1]));
            box.group.element(octaves.group);

            var layers = genModifier("Octave Layers", 13*2);
            layers.group.position([170-40,33+18*2]);
            layers.inp.maxChars(2)
                      .allowed(~/[0-9]/)
                      .text(GLString.make("2", [0,0,0,1]));
            box.group.element(layers.group);

            var upright = genCheckModifier("Upright");
            upright.group.position([160-40,33+18*3]);
            upright.check.toggled(true);
            box.group.element(upright.group);

            box.build = function () {
                var hthreshold = Std.parseFloat(hthreshold.inp.getText().toString());
                var octaves = Std.parseInt(octaves.inp.getText().toString());
                var layers = Std.parseInt(layers.inp.getText().toString());
                var upright = upright.check.getToggled();

                return new SurfDetector(cv.NonFree.surf(
                    hthreshold, octaves, layers, true, upright
                ));
            }

            return box;
        }

        function genDSIFT() {
            var box = new Box(dgraph, 190, 110, "SIFT");

            var features = genModifier("Max Features", 13*3);
            features.group.position([170-40,33]);
            features.inp.maxChars(4)
                        .allowed(~/[0-9]/)
                        .text(GLString.make("100", [0,0,0,1]));
            box.group.element(features.group);

            var layers = genModifier("Octave Layers", 13*2);
            layers.group.position([170-40,33+18]);
            layers.inp.maxChars(2)
                      .allowed(~/[0-9]/)
                      .text(GLString.make("3", [0,0,0,1]));
            box.group.element(layers.group);

            var cthreshold = genModifier("Constrast T.hold", 13*4);
            cthreshold.group.position([170-40,33+18*2]);
            cthreshold.inp.maxChars(5)
                          .allowed(~/[0-9eE+\-]/)
                          .text(GLString.make("0.04", [0,0,0,1]));
            box.group.element(cthreshold.group);

            var ethreshold = genModifier("Edge T.hold", 13*4);
            ethreshold.group.position([170-40,33+18*3]);
            ethreshold.inp.maxChars(5)
                          .allowed(~/[0-9eE+\-]/)
                          .text(GLString.make("10.0", [0,0,0,1]));
            box.group.element(ethreshold.group);

            var sigma = genModifier("σ", 13*4);
            sigma.group.position([170-40,33+18*2]);
            sigma.inp.maxChars(5)
                     .allowed(~/[0-9eE+\-]/)
                     .text(GLString.make("1.6", [0,0,0,1]));
            box.group.element(sigma.group);

            box.build = function () {
                var features = Std.parseInt(features.inp.getText().toString());
                var layers = Std.parseInt(layers.inp.getText().toString());
                var cthreshold = Std.parseFloat(cthreshold.inp.getText().toString());
                var ethreshold = Std.parseFloat(ethreshold.inp.getText().toString());
                var sigma = Std.parseFloat(sigma.inp.getText().toString());

                return new SiftDetector(cv.NonFree.sift(
                    features, layers, cthreshold, ethreshold, sigma
                ));
            }

            return box;
        }

        function genDMasked() {
            var box = new Box(dgraph, 100,65, "Masked", false, 25.0, false, true);

            box.group.element(new PanelButton()
                .fit([15,33,70,23])
                .font(Fonts.dejavu)
                .text(GLString.make("Edit", [1,1,1,1]))
                .press(function (_) {
                    if (box.display != null) return;
                    box.display = Thread.create(MaskedDisplay.main);
                    box.display.sendMessage(Thread.current());
                    box.display.sendMessage(box);
                    if (currentVideo != null)
                        box.display.sendMessage(MsgOpen(currentVideo.extract()));
                    if (currentProcessor != null)
                        box.display.sendMessage(MsgProcessor(currentProcessor.extract()));
                })
                .commit()
            );

            return box;
        }

        function getMiddle(scroll:Dynamic, x:Dynamic):Vec2 {
            var f:Vec4 = scroll.getFit();
            var s:Vec2 = scroll.getScroll();
            var g:Vec4 = x.bounds();
            var p:Vec2 = [f.z/2 - g.z/2, f.w/2 - g.w/2];
            return p - s;
        }
        var w = (550-75)/5;

        var processorPane = new Scroll()
            .fit([0,60,550-75,400-60])
            .hscroll(true)
            .vscroll(true)
            .element(pgraph.group)
            .commit();
        var processorGroup = new Group()
            .position([75,0])
        .element(new Panel()
            .fit([0,60,550-75,400-60])
            .colour([0,0,0,0.8])
            .commit()
        )
        .element(processorPane)
        .element(new PanelButton()
            .fit([-75+75/2-18,400-75/2-18,36,36])
            .radius(18)
            .thickness(2)
            .press(function (_) {
                var processor = pgraph.build();
                if (processor != null) {
                    display.sendMessage(MsgProcessor(cast currentProcessor = processor.extract()));
                }
            })
            .commit())
        .element(new Vector()
            .image(Vectors.refresh)
            .fit([-75+75/2-12,400-75/2-12,24,24])
            .colour([1,1,1,1])
            .commit())
        .element(new Panel()
            .fit([0,60-2,550-75,2])
            .colour([0.5,0.5,0.5,1.0])
            .radius(0)
            .commit());

        processorGroup.element(new PanelButton()
            .fit([0,0,w,30])
            .font(Fonts.dejavu)
            .text(GLString.make("Median Blur", [1,1,1,1]))
            .radius(5)
            .thickness(2)
            .press(function (_) {
                var box = genPMedian();
                box.group.position(getMiddle(processorPane, box.group));
            })
            .commit()
        )
        .element(new PanelButton()
            .fit([w,0,w,30])
            .font(Fonts.dejavu)
            .text(GLString.make("Gaussian Blur", [1,1,1,1]))
            .radius(5)
            .thickness(2)
            .press(function (_) {
                var box = genPGaussian();
                box.group.position(getMiddle(processorPane, box.group));
            })
            .commit()
        )
        .element(new PanelButton()
            .fit([w*2,0,w,30])
            .font(Fonts.dejavu)
            .text(GLString.make("Bilateral Blur", [1,1,1,1]))
            .radius(5)
            .thickness(2)
            .press(function (_) {
                var box = genPBilateral();
                box.group.position(getMiddle(processorPane, box.group));
            })
            .commit()
        )
        .element(new PanelButton()
            .fit([w*3,0,w,30])
            .font(Fonts.dejavu)
            .text(GLString.make("Histogram Eq.", [1,1,1,1]))
            .radius(5)
            .thickness(2)
            .press(function (_) {
                var box = genPHistogram();
                box.group.position(getMiddle(processorPane, box.group));
            })
            .commit()
        )
        .element(new PanelButton()
            .fit([w*4,0,w,30])
            .font(Fonts.dejavu)
            .text(GLString.make("Adaptive Hist.", [1,1,1,1]))
            .radius(5)
            .thickness(2)
            .press(function (_) {
                var box = genPAdaptiveHistogram();
                box.group.position(getMiddle(processorPane, box.group));
            })
            .commit()
        )
        .element(new PanelButton()
            .fit([0,30,w,30])
            .font(Fonts.dejavu)
            .text(GLString.make("Custom (hscript)", [1,1,1,1]))
            .radius(5)
            .thickness(2)
            .press(function (_) {
                var box = genPCustom();
                box.group.position(getMiddle(processorPane, box.group));
            })
            .commit()
        );

        var detectorPane = new Scroll()
            .fit([0,60,550-75,400-60])
            .hscroll(true)
            .vscroll(true)
            .element(dgraph.group)
            .commit();
        var detectorGroup = new Group()
            .position([75,0])
        .element(new Panel()
            .fit([0,60,550-75,400-60])
            .colour([0,0,0,0.8])
            .commit()
        )
        .element(detectorPane)
        .element(new PanelButton()
            .fit([-75+75/2-18,400-75/2-18,36,36])
            .radius(18)
            .thickness(2)
            .press(function (_) {
                var detector = dgraph.build();
                if (detector != null)
                    display.sendMessage(MsgDetector(detector.extract()));
            })
            .commit())
        .element(new Vector()
            .image(Vectors.refresh)
            .fit([-75+75/2-12,400-75/2-12,24,24])
            .colour([1,1,1,1])
            .commit())
        .element(new Panel()
            .fit([0,60-2,550-75,2])
            .colour([0.5,0.5,0.5,1.0])
            .radius(0)
            .commit());

        detectorGroup.element(new PanelButton()
            .fit([0,0,w,30])
            .font(Fonts.dejavu)
            .text(GLString.make("BRISK", [1,1,1,1]))
            .radius(5)
            .thickness(2)
            .press(function (_) {
                var box = genDBrisk();
                box.group.position(getMiddle(detectorPane, box.group));
            })
            .commit()
        )
        .element(new PanelButton()
            .fit([w,0,w,30])
            .font(Fonts.dejavu)
            .text(GLString.make("GFFT", [1,1,1,1]))
            .radius(5)
            .thickness(2)
            .press(function (_) {
                var box = genDGFFT();
                box.group.position(getMiddle(detectorPane, box.group));
            })
            .commit()
        )
        .element(new PanelButton()
            .fit([w*2,0,w,30])
            .font(Fonts.dejavu)
            .text(GLString.make("SURF", [1,1,1,1]))
            .radius(5)
            .thickness(2)
            .press(function (_) {
                var box = genDSURF();
                box.group.position(getMiddle(detectorPane, box.group));
            })
            .commit()
        )
        .element(new PanelButton()
            .fit([w*3,0,w,30])
            .font(Fonts.dejavu)
            .text(GLString.make("SIFT", [1,1,1,1]))
            .radius(5)
            .thickness(2)
            .press(function (_) {
                var box = genDSIFT();
                box.group.position(getMiddle(detectorPane, box.group));
            })
            .commit()
        )
        .element(new PanelButton()
            .fit([w*4,0,w,30])
            .font(Fonts.dejavu)
            .text(GLString.make("Masked", [1,1,1,1]))
            .radius(5)
            .thickness(2)
            .press(function (_) {
                var box = genDMasked();
                box.group.position(getMiddle(detectorPane, box.group));
            })
            .commit()
        );

        var tabbed = new Tabbed("File")
            .pushTab("File", 30, fileGroup)
            .pushTab("Controls")
            .pushTab("Processor", processorGroup)
            .pushTab("Detector", detectorGroup)
            .pushTab("Descriptor")
            .pushTab("Prediction");

        display = Thread.create(Display.main);
        display.sendMessage(Thread.current());
        openDisplays.push(display);

        while (!GLFW.windowShouldClose(window)) {
            var msg:Maybe<Msg> = Thread.readMessage(false);
            if (msg != null) switch (msg.extract()) {
                case MsgBoxEditor(box):
                    box.display = null;
                default:
            }

            GLFW.pollEvents();

            GL.clear(GL.COLOR_BUFFER_BIT);
            glfw.updateState(gui);

            gui.render(bg);
            tabbed.render(gui);

            gui.flush();
            GLFW.swapBuffers(window);
        }

        GLFW.destroyWindow(window);
        gui.destroy();

        while (openDisplays.length > 0) {
            var display = openDisplays.pop();
            closeDisplay(display);
        }

        GLFW.terminate();
    }
}*/

package;

import sys.io.Process;

class Assets {
    static var ttfcompile = "ttfcompile";
    static function main() {
        trace("Generating distance map for refresh.png");
        var p = new Process(ttfcompile, ["-transform", "refresh.png", "30", "32", "-o=refresh.distance"]);

        try {
            var code = p.exitCode();
            if (code != 0) {
                trace("Exited with code="+code);
                trace(p.stderr.readAll());
            }
        }
        catch (e:Dynamic) {
            trace("Exited with exception");
            trace(p.stderr.readAll());
        }
    }
}

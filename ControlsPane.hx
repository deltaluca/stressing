package;

import glgui.*;
import glfw3.*;
import gl3font.*;
import ogl.GLM;
import systools.Dialogs;
import goodies.*;
import ColourScheme;
import Main;
import ColourScheme;

using glgui.Transform;

class ControlsPane {
    public var group:Group;
    public var status:Text;

    public function new() {}

    var buttonWidth:Float;
    var buttonHeight:Float;
    var x:Int;
    var y:Int;
    public function addButton(name:String, press:Void->Void) {
        group.element(new PanelButton()
            .fit([x*buttonWidth, y*buttonHeight, buttonWidth, buttonHeight])
            .font(Fonts.dejavu)
            .text(GLString.make(name, ColourScheme.face))
            .radius(5)
            .thickness(2)
            .press(function (_) press())
            .commit());
        x++;
        if (x >= 6) {
            y++;
            x = 0;
        }
    }

    public dynamic function onReset  ():Void {};
    public dynamic function onTrack  ():Void {};
    public dynamic function onHalt   ():Void {};
    public dynamic function onAnimate():Void {};
    public dynamic function onCompute():Void {};
    public dynamic function onDump   ():Void {};

    public function log(x:String) {
        status.text(status.getText()+x+"\n");
        status.commit();
    }

    public static function create(width:Float, height:Float) {
        var pane = new ControlsPane();
        pane.group = new Group();

        pane.buttonWidth = width/6;
        pane.buttonHeight = 30;
        pane.x = pane.y = 0;

        pane.addButton("Reset",   function () pane.onReset());
        pane.addButton("Track",   function () pane.onTrack());
        pane.addButton("Halt",    function () pane.onHalt());
        pane.addButton("Compute", function () pane.onCompute());
        pane.addButton("Animate", function () pane.onAnimate());
        pane.addButton("Dump",    function () pane.onDump());

        pane.group.element(new Panel()
            .fit([0, pane.buttonHeight, width, height-pane.buttonHeight])
            .colour([0,0,0,0.8])
            .radius(0)
            .commit()
        );
        var sc;
        pane.group.element(sc =new Scroll()
            .fit([0, pane.buttonHeight, width, height-pane.buttonHeight])
            .vscroll(true)
            .element(pane.status = new Text()
                .size(15)
                .font(Fonts.dejavu)
                .halign(TextAlignLeft)
                .valign(TextAlignTop)
                .text(GLString.make(" ",[1,1,1,1]))
                .commit())
            .commit());
        sc.vpercent = 1;

        return pane;
    }
}

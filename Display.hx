package;

import glgui.*;
import glgui.GLFWEventLoop;
import glfw3.*;
import gl3font.*;
import ogl.GL;
import ogl.GLM;
import ogl.GLArray;
import cv.Core;
import cv.core.Mat;
import systools.Dialogs;
import goodies.*;
import ColourScheme;
import #if cpp cpp #else neko #end.vm.Thread;

import Main;
import ImageProcessor;
import FeatureDetector;
import FeatureDescriptor;
import Triangulate;
import Video;
using glgui.Transform;

class Display {

    // Init window settings.
    static inline var windowWidth  = 550;
    static inline var windowHeight = 400;
    static inline var windowTitle  = "Display";

    public static function displayWindow() {
    var m:TMessage = Thread.readMessage(true);
    GLFWEventLoop.track('\033[35mDisplay\033[m ${m}');
    switch (m) {
    case TInit(win):
        GLFWEventLoop.track('@ \033[35mDisplay\033[m SetSize -> GLFWEventLoop');
        win.main.sendMessage(TSetSize (win, windowWidth, windowHeight));
        GLFWEventLoop.track('@ \033[35mDisplay\033[m SetTitle -> GLFWEventLoop');
        win.main.sendMessage(TSetTitle(win, windowTitle));
        GLFWEventLoop.track('@ \033[35mDisplay\033[m MakeVisible -> GLFWEventLoop');
        win.main.sendMessage(TMakeVisible(win));

        GLFW.makeContextCurrent(win.window);
        GL.init();

        GL.enable(GL.BLEND);
        GL.blendFunc(GL.SRC_ALPHA, GL.ONE_MINUS_SRC_ALPHA);

        GL.disable(GL.SCISSOR_TEST);

        GL.viewport(0, 0, windowWidth, windowHeight);

        // --------------------------------------------

        var gui = new Gui();
        var glfw = new GLFWGui(win.window);

        var videoDisplay = new Image()
            .fit([0,18,windowWidth,windowHeight-54]);
        var videoTexture = GL.genTextures(1)[0];
        videoDisplay.texture(videoTexture);

        var tab, processorTab, featuresTab, strainTab;
        var info, featureText, frameText;

        function infoTabs(fname:String, width:Float, height:Float) {
            var x = genTab(width, 18);
            tab = x.tab;
            processorTab = x.processorTab;
            featuresTab  = x.featuresTab;
            strainTab    = x.strainTab;

            var x = genInfo(fname, width, 36);
            info = x.info.position([0,height-36]);
            featureText = x.featureText;
            frameText = x.frameText;
        }
        infoTabs("<no video loaded>", windowWidth, windowHeight);

        // --------------------------------------------

        var heatRender = new Drawing(true);
        heatRender.swapFills();
        var png = PNG.rgba("heat.png");
        var heatTextureData:GLubyteArray = new GLubyteArray(cast png.data);

        var heatTex = GL.genTextures(1)[0];
        GL.bindTexture(GL.TEXTURE_2D, heatTex);
        GL.texImage2D(GL.TEXTURE_2D, 0, GL.RGBA, png.width, png.height, 0, GL.RGBA, heatTextureData);
        GL.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MAG_FILTER, GL.LINEAR);
        GL.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MIN_FILTER, GL.LINEAR);

        // --------------------------------------------

        var currentFrame:Mat = null;
        var nextFrame = true;

        var features:Array<Feature> = [];
        var featureCount = 0;
        var nextFeatures = true;

        var map:Array<PV> = null;
        var triangles:Array<Int> = null;
        var nextMap = true;

        var featureCols:Array<Vec4> = [
            [1.0,0.0,0.0,1.0],
            [0.0,1.0,0.0,1.0],
            [0.0,0.0,1.0,1.0],
            [1.0,0.0,1.0,1.0],
            [0.0,1.0,1.0,1.0],
            [1.0,1.0,0.0,1.0],
        ];
        var videoRender = new Scroll()
        .fit([0,18,windowWidth,windowHeight-54])
        .element(new Custom()
        .apply(function (gui, mpos, proj, xform) {
            var drawing = gui.drawings();
            drawing.setTransform(proj * xform);
            if (featuresTab.getToggled()) {
                for (i in 0...featureCount) {
                    var f = features[i];
                    drawing.drawCircle([f.x,f.y], 2, [1,1,0,1]);
                    for (j in 0...f.basics.length) {
                        var b = f.basics[j];
                        if (b.size > 0)
                            drawing.drawCircle([f.x,f.y], b.size/2, featureCols[j]);
                        if (b.angle >= 0) {}
                    }
                    if (f.vx != null)
                        drawing.drawLine([f.x,f.y],[f.x+f.vx,f.y+f.vy],[1,0,1,1]);
                }
            }
            drawing.flush();

            if (map != null && strainTab.getToggled()) {
                var data = [];
                heatRender.begin(heatTex);
                heatRender.setTransform(proj * xform);
                for (i in 0...Std.int(triangles.length/3)) {
                    var tri = i*3;
                    var f0 = map[triangles[tri+0]];
                    var f1 = map[triangles[tri+1]];
                    var f2 = map[triangles[tri+2]];
                    data.push(f0.x);
                    data.push(f0.y);
                    data.push(f0.u);
                    data.push(0);
                    data.push(0);
                    data.push(1);
                    data.push(f1.x);
                    data.push(f1.y);
                    data.push(f1.u);
                    data.push(0);
                    data.push(0);
                    data.push(1);
                    data.push(f2.x);
                    data.push(f2.y);
                    data.push(f2.u);
                    data.push(0);
                    data.push(0);
                    data.push(1);
                }
                heatRender.data(data);
                heatRender.end();
            }
        }));

        // --------------------------------------------

        while (true) {
        var msg:TMessage = Thread.readMessage(true);
        GLFWEventLoop.track('\033[35mDisplay\033[m ${msg}');
        switch (msg) {
        case TTerminate: break;
        case TOther(x): var y:SMessage = x; switch (y) {
        case SFrame(f):
            currentFrame = f;
            nextFrame = true;
        case SFeatures(fs, cnt):
            features = fs;
            featureCount = cnt;
            nextFeatures = true;
        case SMap(fs, ts):
            map = fs;
            triangles = ts;
            nextMap = true;
        case SSetVideo(video):
            infoTabs(video.fname, video.width, video.height+54);
            GLFWEventLoop.track('@ \033[35mDisplay\033[m SetSize -> GLFWEventLoop');
            win.main.sendMessage(
                TSetSize (win, video.width, video.height+54));
            GL.viewport(0, 0, video.width, video.height+54);
            videoDisplay.fit([0,18,video.width,video.height]);
            videoRender .fit([0,18,video.width,video.height]);

            GL.bindTexture(GL.TEXTURE_2D, videoTexture);
            GL.texImage2D(GL.TEXTURE_2D, 0, GL.LUMINANCE, video.width, video.height, 0, GL.LUMINANCE, null);
            GL.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MAG_FILTER, GL.LINEAR);
            GL.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MIN_FILTER, GL.LINEAR);

            currentFrame = null;
            features = [];
            featureCount = 0;
        default:
        }
        case TUpdate(_):

            if (nextFrame && nextFeatures && nextMap) {
                GLFWEventLoop.track('@ \033[35mDisplay\033[m QueryFrame -> WorkHorse');
                WorkHorse.self.sendMessage(TOther(SQueryFrame(Thread.current(), processorTab.getToggled())));
                GLFWEventLoop.track('@ \033[35mDisplay\033[m QueryFeatures -> WorkHorse');
                WorkHorse.self.sendMessage(TOther(SQueryFeatures(Thread.current())));

                GLFWEventLoop.track('@ \033[35mDisplay\033[m QueryMap -> WorkHorse');
                WorkHorse.self.sendMessage(TOther(SQueryMap(Thread.current())));
                nextFrame = false;
                nextFeatures = false;
                nextMap = false;
            }

            // ----------------------------------------

            GL.clear(GL.COLOR_BUFFER_BIT);
            glfw.updateState(gui);

            featureText.text(GLString.make(
                'features*$featureCount',
                [0.5,0.5,0.9,1]
            )).commit();

            if (currentFrame != null) {
                VideoUtils.subData(videoTexture, currentFrame);
                gui.render(videoDisplay);
            }

            gui.render(videoRender);
            gui.render(tab);
            gui.render(info);

            gui.flush();
            GLFW.swapBuffers(win.window);

            // ----------------------------------------

        GLFWEventLoop.track('@ \033[35mDisplay\033[m Continue -> GLFWEventLoop');
        win.main.sendMessage(TContinue(win)); default: }
        }

        // --------------------------------------------

        gui.destroy();
        GLFWEventLoop.track('@ \033[35mDisplay\033[m CloseWindow -> GLFWEventLoop');
        win.main.sendMessage(TCloseWindow(win));

        GLFWEventLoop.track('%\033[35mDisplay\033[m TERMINATED');
    default: throw "Display Nooo"; }}

    static function genInfo(fname:String, width:Float, height:Float) {
        var half = 2+(height-2)/2;
        var featureText;
        var frameText;
        var info = new Group()
        .element(new Panel()
            .fit([0,0,width,2])
            .colour(ColourScheme.border)
            .radius(0)
            .commit())
        .element(new Text()
            .position([0, half])
            .size(15)
            .text(GLString.make(fname, ColourScheme.fileColour))
            .font(Fonts.dejavu)
            .halign(TextAlignLeft)
            .valign(TextAlignTop)
            .commit())
        .element(featureText = new Text()
            .position([0, 2])
            .size(15)
            .font(Fonts.dejavu)
            .text(GLString.make("features*0", [0,0,0,0]))
            .halign(TextAlignLeft)
            .valign(TextAlignTop)
            .commit())
        .element(frameText = new Text()
            .position([width, half])
            .size(15)
            .font(Fonts.dejavu)
            .text(GLString.make(" ",[0,0,0,0]))
            .halign(TextAlignRight)
            .valign(TextAlignTop)
            .commit());
        return {
            info: info,
            featureText: featureText,
            frameText: frameText
        };
    }

    static function genTab(width:Float, height:Float) {
        var processorTab;
        var featuresTab;
        var strainTab;
        var tab = new Group()
        .element(new Panel()
            .fit([0,height-2,width,2])
            .colour(ColourScheme.border)
            .radius(0)
            .commit())
        .element(processorTab = new PanelButton(true)
            .fit([0,0,80,height-2])
            .colour(ColourScheme.base)
            .overColour(ColourScheme.brightenMask)
            .pressColour(ColourScheme.base)
            .borderColour(ColourScheme.base)
            .radius(0)
            .thickness(0)
            .font(Fonts.dejavu)
            .text(GLString.make("Processed", ColourScheme.tabColour))
            .disabledText(GLString.make("Processed", ColourScheme.disabledTabColour))
            .toggled(true)
            .commit())
        .element(featuresTab = new PanelButton(true)
            .fit([80,0,80,height-2])
            .colour(ColourScheme.base)
            .overColour(ColourScheme.brightenMask)
            .pressColour(ColourScheme.base)
            .borderColour(ColourScheme.base)
            .radius(0)
            .thickness(0)
            .font(Fonts.dejavu)
            .text(GLString.make("Features", ColourScheme.tabColour))
            .disabledText(GLString.make("Features", ColourScheme.disabledTabColour))
            .toggled(true)
            .commit())
        .element(strainTab = new PanelButton(true)
            .fit([160,0,80,height-2])
            .colour(ColourScheme.base)
            .overColour(ColourScheme.brightenMask)
            .pressColour(ColourScheme.base)
            .borderColour(ColourScheme.base)
            .radius(0)
            .thickness(0)
            .font(Fonts.dejavu)
            .text(GLString.make("Strain Map", ColourScheme.tabColour))
            .disabledText(GLString.make("Strain Map", ColourScheme.disabledTabColour))
            .toggled(true)
            .commit());
        return {
            tab: tab,
            processorTab: processorTab,
            featuresTab : featuresTab,
            strainTab   : strainTab
        };
    }

}



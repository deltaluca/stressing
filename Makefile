all:
	haxe -main Main -neko Main.n -D HXCPP_M64 -D glgui_tracks \
    -lib hscript -lib glfw3 -lib glgui -lib opencv -lib systools
	nekotools boot Main.n
#	./Main -video=0 -config=data/config.json
	./Main -video=data/2011.06.30-a1_5cm.avi -config=data/config_2011.json
	./Main -video=data/0303_11M.avi -config=data/config2.json

debug:
	haxe -main Main -cpp bin -D HXCPP_M64 \
    -lib hscript -lib glfw3 -lib glgui -lib opencv -lib systools
	./bin/Main
#	./bin-debug/Main-debug

assets:
	haxe -x Assets.hx

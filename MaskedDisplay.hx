package;

import glgui.*;
import glgui.GLFWEventLoop;
import glfw3.*;
import gl3font.*;
import ogl.GL;
import ogl.GLM;
import cv.Core;
import cv.core.Mat;
import systools.Dialogs;
import goodies.*;
import ColourScheme;
import #if cpp cpp #else neko #end.vm.Thread;

import Main;
import ImageProcessor;
import FeatureDetector;
import FeatureDescriptor;
import Video;
using glgui.Transform;

class MaskedDisplay {

    // Init window settings.
    static inline var windowWidth  = 550;
    static inline var windowHeight = 400;
    static inline var windowTitle  = "Mask Editor";

    public static function displayWindow() {
    var m:TMessage = Thread.readMessage(true);
    GLFWEventLoop.track('\033[34mMaskedDisplay\033[m ${m}');
    switch (m) {
    case TInit(win):
        GLFWEventLoop.track('@ \033[34mMaskedDisplay\033[m SetSize -> GLFWEventLoop');
        win.main.sendMessage(TSetSize (win, windowWidth, windowHeight));
        GLFWEventLoop.track('@ \033[34mMaskedDisplay\033[m SetTitle -> GLFWEventLoop');
        win.main.sendMessage(TSetTitle(win, windowTitle));
        GLFWEventLoop.track('@ \033[34mMaskedDisplay\033[m MakeVisible -> GLFWEventLoop');
        win.main.sendMessage(TMakeVisible(win));

        GLFW.makeContextCurrent(win.window);
        GL.init();

        GL.enable(GL.BLEND);
        GL.blendFunc(GL.SRC_ALPHA, GL.ONE_MINUS_SRC_ALPHA);

        GL.disable(GL.SCISSOR_TEST);

        GL.viewport(0, 0, windowWidth, windowHeight);

        // --------------------------------------------

        var callbackThread:Thread = null;

        var gui = new Gui();
        var glfw = new GLFWGui(win.window);

        var videoDisplay = new Image()
            .fit([0,18,windowWidth,windowHeight-36]);
        var videoTexture = GL.genTextures(1)[0];
        videoDisplay.texture(videoTexture);

        var tab, processorTab;
        var info, pointText;

        function infoTabs(fname:String, width:Float, height:Float) {
            var x = genTab(width, 18);
            tab = x.tab;
            processorTab = x.processorTab;

            var x = genInfo(fname, width, 18);
            info = x.info.position([0,height-18]);
            pointText = x.point;
        }
        infoTabs("<no video loaded>", windowWidth, windowHeight);

        // --------------------------------------------

        var currentFrame:Mat = null;
        var nextFrame = true;

        var masks:Array<Array<Vec2>> = null;

        var colours:Array<Vec4> = [
            [1.0,0.2,0.2,1.0],
            [0.0,1.0,0.0,1.0],
            [0.4,0.4,1.0,1.0],
            [1.0,1.0,0.0,1.0],
            [1.0,0.0,1.0,1.0],
            [0.0,1.0,1.0,1.0]
        ];

        function project(p:Vec2) {
            var f = videoDisplay.getFit();
            return p - new Vec2([f.x,f.y]);
        }
        function unproject(p:Vec2) {
            var f = videoDisplay.getFit();
            return p + new Vec2([f.x,f.y]);
        }

        function getMask(p:Vec2):{mask:Array<Vec2>,point:Vec2} {
            var closest = null;
            var cdist:Float = 0;
            if (masks != null)
                for (mask in masks) {
                    for (m in mask) {
                        var d = Vec2.distance(p, unproject(m));
                        if (d > 7) continue;
                        if (d < cdist || closest == null) {
                            cdist = d;
                            closest = {mask:mask, point:m};
                        }
                    }
                }
            return closest;
        }

        var closest = null;
        var editing = false;
        var currentMask = null;

        var mouse = new Mouse()
            .interior(videoDisplay.internal)
            .mouse(function (pos) {
                var fit = videoDisplay.getFit();
                if (pos == null) return;
                var pos = pos.extract();
                var proj = project(pos);
                if (proj.x >= 0 && proj.y >= 0 && proj.x < fit.z && proj.y < fit.w)
                    pointText.text(GLString.make(
                        '(${Std.int(pos.x)},${Std.int(pos.y-18)})', ColourScheme.face
                    )).commit();
                else
                    pointText.text(GLString.make(" ",[0,0,0,0])).commit();
                if (!editing) closest = getMask(pos);
                else {
                    if (proj.x < 0) proj.x = 0;
                    if (proj.y < 0) proj.y = 0;
                    if (proj.x > fit.z) proj.x = fit.z;
                    if (proj.y > fit.w) proj.y = fit.w;
                    closest.point.x = proj.x;
                    closest.point.y = proj.y;
                }
            })
            .press(function (pos, but) {
                if (pos == null) return;
                var pos = project(pos.extract());
                if (Type.enumEq(but, MouseLeft)) {
                    if (closest == null) {
                        if (currentMask == null) {
                            currentMask = [];
                            masks.push(currentMask);
                        }
                        currentMask.push(pos);
                    }
                    else {
                        editing = true;
                        currentMask = closest.mask;
                    }
                }
                else if (Type.enumEq(but, MouseRight)) {
                    if (closest == null) {
                        currentMask = [];
                        masks.push(currentMask);
                        currentMask.push(pos);
                    }
                    else {
                        closest.mask.remove(closest.point);
                        if (closest.mask.length == 0) {
                            masks.remove(closest.mask);
                            currentMask = null;
                        } else currentMask = closest.mask;
                        closest = getMask(pos);
                    }
                }
            })
            .release(function (pos, but, _) {
                if (Type.enumEq(but, MouseLeft))
                    editing = false;
            })
            .commit();

        // --------------------------------------------

        while (true) {
        var msg:TMessage = Thread.readMessage(true);
        GLFWEventLoop.track('\033[34mMaskedDisplay\033[m ${msg}');
        switch (msg) {
        case TTerminate: break;
        case TOther(x): var y:SMessage = x; switch (y) {
        case SCallback(t):
            callbackThread = t;
        case SData(_,t):
            masks = t;
        case SFrame(f):
            currentFrame = f;
            nextFrame = true;
        case SSetVideo(video):
            infoTabs(video.fname, video.width, video.height+36);
            GLFWEventLoop.track('@ \033[34mMaskedDisplay\033[m SetSize -> GLFWEventLoop');
            win.main.sendMessage(
                TSetSize (win, video.width, video.height+36));
            GL.viewport(0, 0, video.width, video.height+36);
            videoDisplay.fit([0,18,video.width,video.height]);

            GL.bindTexture(GL.TEXTURE_2D, videoTexture);
            GL.texImage2D(GL.TEXTURE_2D, 0, GL.LUMINANCE, video.width, video.height, 0, GL.LUMINANCE, null);
            GL.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MAG_FILTER, GL.LINEAR);
            GL.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MIN_FILTER, GL.LINEAR);
        default:
        }
        case TUpdate(shouldClose):
            if (shouldClose && callbackThread != null) {
                GLFWEventLoop.track('@ \033[34mMaskedDisplay\033[m ClosedDisplay -> [callback]');
                callbackThread.sendMessage(TOther(SClosedDisplay(win)));
                break;
            }

            if (nextFrame) {
                GLFWEventLoop.track('@ \033[34mMaskedDisplay\033[m QueryFrame -> WorkHorse');
                WorkHorse.self.sendMessage(TOther(SQueryFrame(Thread.current(), processorTab.getToggled())));
                nextFrame = false;
            }

            // ----------------------------------------

            GL.clear(GL.COLOR_BUFFER_BIT);
            glfw.updateState(gui);

            if (currentFrame != null)
                VideoUtils.subData(videoTexture, currentFrame);

            gui.render(videoDisplay);
            gui.render(tab);
            gui.render(info);
            gui.render(mouse);

            var drawing = gui.drawings();
            drawing.setTransform(gui.projection());
            var i = 0;
            if (masks != null) for (mask in masks) {
                var col = colours[(i = (i+1)%colours.length)];
                var cola = col * new Vec4([1,1,1,0.5]);
                var pre = null;
                for (q in mask) {
                    var p = unproject(q);
                    var rad = 2;
                    if (closest != null && !editing && closest.point == q)
                        rad = 6;
                    drawing.drawCircle(p, rad, col);
                    if (pre != null)
                        drawing.drawLine(pre, p, cola);
                    pre = p;
                }
                if (mask.length>1)
                    drawing.drawDashedLine(pre, unproject(mask[0]), cola, 6, 3);
            }

            gui.flush();
            GLFW.swapBuffers(win.window);

            // ----------------------------------------

        GLFWEventLoop.track('@ \033[34mMaskedDisplay\033[m Continue -> GLFWEventLoop');
        win.main.sendMessage(TContinue(win)); default: }
        }

        // --------------------------------------------

        gui.destroy();
        GLFWEventLoop.track('@ \033[34mMaskedDisplay\033[m CloseWindow -> GLFWEventLoop');
        win.main.sendMessage(TCloseWindow(win));

        GLFWEventLoop.track('%\033[34mMaskedDisplay\033[m TERMINATED');
    default: }}

    static function genInfo(fname:String, width:Float, height:Float) {
        var point;
        var info = new Group()
        .element(new Panel()
            .fit([0,0,width,2])
            .colour(ColourScheme.border)
            .radius(0)
            .commit())
        .element(new Text()
            .position([0, 0])
            .size(15)
            .text(GLString.make(fname, ColourScheme.fileColour))
            .font(Fonts.dejavu)
            .halign(TextAlignLeft)
            .valign(TextAlignTop)
            .commit())
        .element(point = new Text()
            .position([width,0])
            .size(15)
            .font(Fonts.dejavu)
            .text(GLString.make(" ",[0,0,0,0]))
            .halign(TextAlignRight)
            .valign(TextAlignTop)
            .commit());
        return {
            info: info,
            point: point
        };
    }

    static function genTab(width:Float, height:Float) {
        var processorTab;
        var tab = new Group()
        .element(new Panel()
            .fit([0,height-2,width,2])
            .colour(ColourScheme.border)
            .radius(0)
            .commit())
        .element(processorTab = new PanelButton(true)
            .fit([0,0,80,height-2])
            .colour(ColourScheme.base)
            .overColour(ColourScheme.brightenMask)
            .pressColour(ColourScheme.base)
            .borderColour(ColourScheme.base)
            .radius(0)
            .thickness(0)
            .font(Fonts.dejavu)
            .text(GLString.make("Processed", ColourScheme.tabColour))
            .disabledText(GLString.make("Processed", ColourScheme.disabledTabColour))
            .toggled(true)
            .commit());
        return {
            tab: tab,
            processorTab: processorTab
        };
    }

}


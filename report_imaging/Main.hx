package;
import cv.Core;
import cv.HighGUI;
import cv.ImgProc;
import cv.core.Mat;
import format.png.Tools;
import format.png.Writer;
import glgui.Gui;
import glgui.Image;
import glfw3.GLFW;
import ogl.GL;
import ogl.GLArray;

class Main {
    static inline var path = "../data/2011.06.30-a1_5cm.avi";
    static function main() {
        var args = Sys.args();
        var arg = args.length == 0 ? "nada" : args[0];

        Core.init();
        var video  = HighGUI.captureFromFile(path);
        var width  = Std.int(HighGUI.getCaptureProperty(video, HighGUI.CAP_PROP_FRAME_WIDTH));
        var height = Std.int(HighGUI.getCaptureProperty(video, HighGUI.CAP_PROP_FRAME_HEIGHT));

        var image = HighGUI.queryFrame(video);
        var frame = Core.createMat(height, width, Core._8U);
        ImgProc.cvtColor(image, frame, ImgProc.RGB2GRAY);
        var out = Core.cloneMat(frame);

        GLFW.init();
        var w = GLFW.createWindow(width, height, arg);
        GLFW.makeContextCurrent(w);
        GL.init();

        function p(frame:Mat, out:Mat, arg:String) {
            switch (arg) {
            case "median":
                ImgProc.medianBlur(frame, out, 3);
            case "gaussian":
                ImgProc.gaussianBlur(frame, out, Core.size(3,3), 0.0);
            case "bilateral":
                ImgProc.bilateralFilter(frame, out, 9, 50, 50);
            case "histogram":
                ImgProc.equalizeHist(frame, out);
            case "adaptive":
                ImgProc.equalizeHistAdaptive(frame, out, 25, 25, 0.025);
            case "pyramid":
                ImgProc.equalizeHistAdaptive(frame, out, 50, 50, 0.025/4);
                ImgProc.equalizeHistAdaptive(out, frame, 25, 25, 0.025);
                ImgProc.equalizeHistAdaptive(frame, out, 12, 12, 0.025/4);
                ImgProc.equalizeHist(out, frame);
                Core.copy(frame, out);
            case "threshold":
                ImgProc.threshold(frame, out, 25, 0, ImgProc.THRESH_TOZERO);
            case "combi":
                ImgProc.threshold(frame, out, 25, 0, ImgProc.THRESH_TOZERO);
                Core.copy(out, frame);
                ImgProc.bilateralFilter(frame, out, 5, 10, 10);
                Core.copy(out, frame);
                ImgProc.equalizeHistAdaptive(frame, out, 50, 50, 0.025/4);
                ImgProc.equalizeHistAdaptive(out, frame, 25, 25, 0.025);
                ImgProc.equalizeHistAdaptive(frame, out, 12, 12, 0.025);
                ImgProc.equalizeHist(out, frame);
                Core.copy(frame, out);
            default:
                Core.copy(frame, out);
            }
        };
        p(frame, out, arg);

        var bytes = new GLubyteArray(cast out.raw);
        var hbytes = new haxe.io.BytesOutput();
        for (y in 0...height) {
            for (x in 0...(width>>1))
                hbytes.writeByte(0xff-bytes[y*width + x]);
        }
        var f = sys.io.File.write('$arg.png', true);
        var writer = new Writer(f);
        writer.write(Tools.buildGrey(width>>1, height, hbytes.getBytes()));
        f.flush();
        f.close();

        var tex = GL.genTextures(1)[0];
        GL.bindTexture(GL.TEXTURE_2D, tex);
        GL.texImage2D(GL.TEXTURE_2D, 0, GL.LUMINANCE, width, height, 0, GL.LUMINANCE, bytes);
        GL.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MAG_FILTER, GL.LINEAR);
        GL.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MIN_FILTER, GL.LINEAR);

        var gui = new glgui.Gui()
             .screen([width,height]);
        var image = new glgui.Image();
        image.fit([0,0,width,height])
             .texture(tex);

        while (!GLFW.windowShouldClose(w)) {
            GLFW.waitEvents();

            gui.render(image);
            gui.flush();

            GLFW.swapBuffers(w);
        }

        GLFW.destroyWindow(w);
        GLFW.terminate();
    }
}

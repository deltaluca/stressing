package;

import glgui.*;
import glfw3.*;
import gl3font.*;
import ogl.GLM;
import systools.Dialogs;
import goodies.*;
import ColourScheme;
import Main;
import ColourScheme;

using glgui.Transform;

typedef TrackingInfo = {
    smooth : Float,
    window : Int,
    pyramids : Int,
    iterations : Int,
    maxError : Float
};

class TrackingPane {
    public var group:Group;
    public function new() {}

    var window    :{group:Group,inp:TextInput};
    var pyramids  :{group:Group,inp:TextInput};
    var iterations:{group:Group,inp:TextInput};
    var smooth    :{group:Group,inp:TextInput};
    var error     :{group:Group,inp:TextInput};
    public static function create(width:Float, height:Float) {
        var pane = new TrackingPane();
        pane.group = new Group();

        var window = pane.window = ConfigPane.genModifier("Window Size", 13*3, ColourScheme.face);
        window.group.position([150,50]);
        window.inp.maxChars(4)
                  .allowed(~/[0-9]/)
                  .text(GLString.make("21", ColourScheme.base))
                  .commit();
        pane.group.element(window.group);

        var pyramids = pane.pyramids = ConfigPane.genModifier("Num Pyramids", 13*3, ColourScheme.face);
        pyramids.group.position([150,50+20]);
        pyramids.inp.maxChars(4)
                  .allowed(~/[0-9]/)
                  .text(GLString.make("3", ColourScheme.base))
                  .commit();
        pane.group.element(pyramids.group);

        var iterations = pane.iterations = ConfigPane.genModifier("Iterations", 13*3, ColourScheme.face);
        iterations.group.position([150,50+20*2]);
        iterations.inp.maxChars(4)
                  .allowed(~/[0-9]/)
                  .text(GLString.make("20", ColourScheme.base))
                  .commit();
        pane.group.element(iterations.group);

        var smooth = pane.smooth = ConfigPane.genModifier("Smoothening", 13*4, ColourScheme.face);
        smooth.group.position([150,50+20*3]);
        smooth.inp.maxChars(4)
                  .allowed(~/[0-9\.eE+\-]/)
                  .text(GLString.make("0.8", ColourScheme.base))
                  .commit();
        pane.group.element(smooth.group);

        var error = pane.error = ConfigPane.genModifier("Max Error", 13*4, ColourScheme.face);
        error.group.position([150,50+20*4]);
        error.inp.maxChars(4)
                  .allowed(~/[0-9\.eE+\-]/)
                  .text(GLString.make("20", ColourScheme.base))
                  .commit();
        pane.group.element(error.group);

        return pane;
    }

    public function parse(info:TrackingInfo) {
        if (info == null) return;
        window.inp.text(GLString.make(Std.string(info.window), ColourScheme.base)).commit();
        iterations.inp.text(GLString.make(Std.string(info.iterations), ColourScheme.base)).commit();
        error.inp.text(GLString.make(Std.string(info.maxError), ColourScheme.base)).commit();
        pyramids.inp.text(GLString.make(Std.string(info.pyramids), ColourScheme.base)).commit();
        smooth.inp.text(GLString.make(Std.string(info.smooth), ColourScheme.base)).commit();
    }

    public function info():TrackingInfo {
        return {
            window    : Std.parseInt(window.inp.getText().toString()),
            smooth    : Std.parseFloat(smooth.inp.getText().toString()),
            pyramids  : Std.parseInt(pyramids.inp.getText().toString()),
            iterations: Std.parseInt(iterations.inp.getText().toString()),
            maxError  : Std.parseFloat(error.inp.getText().toString())
        };
    }
}

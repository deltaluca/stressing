package;

import cv.core.Mat;
import goodies.Maybe;

import ogl.GL;
import ogl.GLM;
import ogl.GLArray;

interface Video {
    public var width:Int;
    public var height:Int;
    public var fname:String;

    public function restart():Void;
    public function frame(n:Int):Maybe<Mat>;
    public function update():Maybe<Mat>;
}

class VideoStore {
    var frames:Array<Mat>;
    public function new() {
        frames = [];
    }
    public function push(m:Mat) {
        frames.push(m);
    }
    public function get(n:Int) {
        if (n >= frames.length) return null;
        return frames[n];
    }
}

class VideoUtils {
    static public function subData(tex:GLuint, mat:Mat) {
        GL.bindTexture(GL.TEXTURE_2D, tex);
        var data:GLubyteArray = new GLubyteArray(cast mat.raw);
        GL.texSubImage2D(GL.TEXTURE_2D, 0, 0,0, mat.cols, mat.rows, GL.LUMINANCE, data);
    }
}

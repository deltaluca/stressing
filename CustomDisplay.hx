package;

import glgui.*;
import glgui.GLFWEventLoop;
import glfw3.*;
import gl3font.*;
import ogl.GL;
import ogl.GLM;
import cv.Core;
import cv.core.Mat;
import systools.Dialogs;
import goodies.*;
import ColourScheme;
import #if cpp cpp #else neko #end.vm.Thread;

import Main;
import ImageProcessor;
import FeatureDetector;
import FeatureDescriptor;
import Video;
using glgui.Transform;

class CustomDisplay {

    // Init window settings.
    static inline var windowWidth  = 550;
    static inline var windowHeight = 400;
    static inline var windowTitle  = "Custom Editor";

    public static function displayWindow() {
    var m:TMessage = Thread.readMessage(true);
    GLFWEventLoop.track('\033[34mCustomDisplay\033[m ${m}');
    switch (m) {
    case TInit(win):
        GLFWEventLoop.track('@ \033[34mCustomDisplay\033[m SetSize -> GLFWEventLoop');
        win.main.sendMessage(TSetSize (win, windowWidth, windowHeight));
        GLFWEventLoop.track('@ \033[34mCustomDisplay\033[m SetTitle -> GLFWEventLoop');
        win.main.sendMessage(TSetTitle(win, windowTitle));
        GLFWEventLoop.track('@ \033[34mCustomDisplay\033[m MakeVisible -> GLFWEventLoop');
        win.main.sendMessage(TMakeVisible(win));

        GLFW.makeContextCurrent(win.window);
        GL.init();

        GL.enable(GL.BLEND);
        GL.blendFunc(GL.SRC_ALPHA, GL.ONE_MINUS_SRC_ALPHA);

        GL.disable(GL.SCISSOR_TEST);

        GL.viewport(0, 0, windowWidth, windowHeight);

        // --------------------------------------------

        var callbackThread:Thread = null;

        var gui = new Gui();
        var glfw = new GLFWGui(win.window);

        var videoDisplay = new Image()
            .fit([0,18,windowWidth,windowHeight-36]);
        var videoTexture = GL.genTextures(1)[0];
        videoDisplay.texture(videoTexture);

        var tab, processorTab;
        var info, pointText;

        function infoTabs(fname:String, width:Float, height:Float) {
            var x = genTab(width, 18);
            tab = x.tab;
            processorTab = x.processorTab;

            var x = genInfo(fname, width, 18);
            info = x.info.position([0,height-18]);
            pointText = x.point;
        }
        infoTabs("<no video loaded>", windowWidth, windowHeight);

        // --------------------------------------------

        var currentFrame:Mat = null;
        var nextFrame = true;

        var features:Array<{x:Float,y:Float,r:Float,a:Null<Float>}> = null;

        function project(p:Vec2) {
            var f = videoDisplay.getFit();
            return p - new Vec2([f.x,f.y]);
        }
        function unproject(p:Vec2) {
            var f = videoDisplay.getFit();
            return p + new Vec2([f.x,f.y]);
        }

        function getFeature(p:Vec2) {
            var closest = null;
            var cdist:Float = 0;
            var cr:Float = 0;
            if (features != null) for (f in features) {
                var d = Vec2.distance(p, unproject([f.x,f.y]));
                if (d > Math.max(4,f.r)) continue;
                if (d < cdist || f.r < cr || closest == null) {
                    cr = f.r;
                    cdist = d;
                    closest = f;
                }
            }
            return closest;
        }

        var closest = null;
        var editing = false;
        var currentMask = null;

        var mouse = new Mouse()
            .interior(videoDisplay.internal)
            .mouse(function (pos) {
                var fit = videoDisplay.getFit();
                if (pos == null) return;
                var pos = pos.extract();
                var proj = project(pos);
                if (proj.x >= 0 && proj.y >= 0 && proj.x < fit.z && proj.y < fit.w)
                    pointText.text(GLString.make(
                        '(${Std.int(pos.x)},${Std.int(pos.y-18)})', ColourScheme.face
                    )).commit();
                else
                    pointText.text(GLString.make(" ",[0,0,0,0])).commit();
                if (!editing) closest = getFeature(pos);
                else {
                    var del = proj - new Vec2([closest.x,closest.y]);
                    if (GLFW.getKey(win.window, GLFW.LEFT_CONTROL)) {
                        closest.r = Vec2.length(del);
                    }
                    else if (GLFW.getKey(win.window, GLFW.LEFT_SHIFT)) {
                        closest.a = Math.atan2(del.y, del.x);
                    }
                    else {
                        if (proj.x < 0) proj.x = 0;
                        if (proj.y < 0) proj.y = 0;
                        if (proj.x > fit.z) proj.x = fit.z;
                        if (proj.y > fit.w) proj.y = fit.w;
                        closest.x = proj.x;
                        closest.y = proj.y;
                    }
                }
            })
            .press(function (pos, but) {
                if (pos == null) return;
                var pos = project(pos.extract());
                if (Type.enumEq(but, MouseLeft)) {
                    if (closest == null) {
                        features.push(closest = {
                            x: pos.x,
                            y: pos.y,
                            r: 0,
                            a: null
                        });
                        editing = true;
                    }
                    else
                        editing = true;
                }
                else if (Type.enumEq(but, MouseRight)) {
                    if (closest != null) {
                        if (GLFW.getKey(win.window, GLFW.LEFT_SHIFT)) {
                            closest.a = null;
                        }
                        else {
                            features.remove(closest);
                            closest = getFeature(pos);
                        }
                    }
                }
            })
            .release(function (pos, but, _) {
                if (Type.enumEq(but, MouseLeft))
                    editing = false;
            })
            .commit();

        // --------------------------------------------

        while (true) {
        var msg:TMessage = Thread.readMessage(true);
        GLFWEventLoop.track('\033[34mCustomDisplay\033[m ${msg}');
        switch (msg) {
        case TTerminate: break;
        case TOther(x): var y:SMessage = x; switch (y) {
        case SCallback(t):
            callbackThread = t;
        case SData(_,t):
            features = t;
        case SFrame(f):
            currentFrame = f;
            nextFrame = true;
        case SSetVideo(video):
            infoTabs(video.fname, video.width, video.height+36);
            GLFWEventLoop.track('@ \033[34mCustomDisplay\033[m SetSize -> GLFWEventLoop');
            win.main.sendMessage(
                TSetSize (win, video.width, video.height+36));
            GL.viewport(0, 0, video.width, video.height+36);
            videoDisplay.fit([0,18,video.width,video.height]);

            GL.bindTexture(GL.TEXTURE_2D, videoTexture);
            GL.texImage2D(GL.TEXTURE_2D, 0, GL.LUMINANCE, video.width, video.height, 0, GL.LUMINANCE, null);
            GL.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MAG_FILTER, GL.LINEAR);
            GL.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MIN_FILTER, GL.LINEAR);
        default:
        }
        case TUpdate(shouldClose):
            if (shouldClose && callbackThread != null) {
                GLFWEventLoop.track('@ \033[34mCustomDisplay\033[m ClosedDisplay -> [callback]');
                callbackThread.sendMessage(TOther(SClosedDisplay(win)));
                break;
            }

            if (nextFrame) {
                GLFWEventLoop.track('@ \033[34mCustomDisplay\033[m QueryFrame -> WorkHorse');
                WorkHorse.self.sendMessage(TOther(SQueryFrame(Thread.current(), processorTab.getToggled())));
                nextFrame = false;
            }

            // ----------------------------------------

            GL.clear(GL.COLOR_BUFFER_BIT);
            glfw.updateState(gui);

            if (currentFrame != null)
                VideoUtils.subData(videoTexture, currentFrame);

            gui.render(videoDisplay);
            gui.render(tab);
            gui.render(info);
            gui.render(mouse);

            var drawing = gui.drawings();
            drawing.setTransform(gui.projection());
            if (features != null) for (f in features) {
                var p = unproject([f.x,f.y]);
                var rad = 1.5;
                if (f == closest) rad = 4;
                drawing.drawCircle(p,rad,[1,0,0,1]);
                drawing.drawCircle(p,f.r,[1,1,0,1]);
                if (f.a != null) {
                    var a:Vec2 = [Math.cos(f.a), Math.sin(f.a)];
                    drawing.drawLine(p,p+a*f.r,[0,1,0,1]);
                }
            }

            gui.flush();
            GLFW.swapBuffers(win.window);

            // ----------------------------------------

        GLFWEventLoop.track('@ \033[34mCustomDisplay\033[m Continue -> GLFWEventLoop');
        win.main.sendMessage(TContinue(win)); default: }
        }

        // --------------------------------------------

        gui.destroy();
        GLFWEventLoop.track('@ \033[34mCustomDisplay\033[m CloseWindow -> GLFWEventLoop');
        win.main.sendMessage(TCloseWindow(win));

        GLFWEventLoop.track('%\033[34mCustomDisplay\033[m TERMINATED');
    default: throw "CustomDisplay Nooo"; }}

    static function genInfo(fname:String, width:Float, height:Float) {
        var point;
        var info = new Group()
        .element(new Panel()
            .fit([0,0,width,2])
            .colour(ColourScheme.border)
            .radius(0)
            .commit())
        .element(new Text()
            .position([0, 0])
            .size(15)
            .text(GLString.make(fname, ColourScheme.fileColour))
            .font(Fonts.dejavu)
            .halign(TextAlignLeft)
            .valign(TextAlignTop)
            .commit())
        .element(point = new Text()
            .position([width,0])
            .size(15)
            .font(Fonts.dejavu)
            .text(GLString.make(" ",[0,0,0,0]))
            .halign(TextAlignRight)
            .valign(TextAlignTop)
            .commit());
        return {
            info: info,
            point: point
        };
    }

    static function genTab(width:Float, height:Float) {
        var processorTab;
        var tab = new Group()
        .element(new Panel()
            .fit([0,height-2,width,2])
            .colour(ColourScheme.border)
            .radius(0)
            .commit())
        .element(processorTab = new PanelButton(true)
            .fit([0,0,80,height-2])
            .colour(ColourScheme.base)
            .overColour(ColourScheme.brightenMask)
            .pressColour(ColourScheme.base)
            .borderColour(ColourScheme.base)
            .radius(0)
            .thickness(0)
            .font(Fonts.dejavu)
            .text(GLString.make("Processed", ColourScheme.tabColour))
            .disabledText(GLString.make("Processed", ColourScheme.disabledTabColour))
            .toggled(true)
            .commit());
        return {
            tab: tab,
            processorTab: processorTab
        };
    }

}



package;

import glgui.*;
import glgui.GLFWEventLoop;
import glfw3.*;
import gl3font.*;
import ogl.GLM;
import systools.Dialogs;
import goodies.*;
import #if cpp cpp #else neko #end.vm.Thread;
import Main;
import ColourScheme;
import ConfigPane;
import FeatureDetector;

using glgui.Transform;

typedef JDetector = {
    type : String
};
typedef JDBrisk = { > JDetector,
    threshold : Int,
    octaves   : Int,
    scale     : Float
};
typedef JDGood = { > JDetector,
    features : Int,
    quality  : Float,
    distance : Float,
    block    : Int,
    harris   : Bool,
    param    : Float
};
typedef JDSurf = { > JDetector,
    hthreshold : Float,
    octaves    : Int,
    layers     : Int,
    upright    : Bool
};
typedef JDSift = { > JDetector,
    features   : Int,
    layers     : Int,
    cthreshold : Float,
    ethreshold : Float,
    sigma      : Float
};
typedef JDMasked = { > JDetector,
    sub : Array<JDetector>,
    masks : Array<Array<{x:Float,y:Float}>>
};
typedef JDCustom = { > JDetector,
    features : Array<{
        x:Float,
        y:Float,
        r:Float,
        a:Null<Float>
    }>
};

class DetectorPane {
    public static function create(width:Float, height:Float) {
        var pane = new ConfigPane<FeatureDetector>(width, height);
        pane.addButton("BRISK",  createBrisk .bind(_,null));
        pane.addButton("GFFT",   createGood  .bind(_,null));
        pane.addButton("SURF",   createSurf  .bind(_,null));
        pane.addButton("SIFT",   createSift  .bind(_,null));
        pane.addButton("Masked", createMasked.bind(_,null));
        pane.addButton("Custom", createCustom.bind(_,null));
        pane.graph.multi = function (xs)
            return new ConcatDetector(xs);
        return pane;
    }

    public static function parse(pane:ConfigPane<FeatureDetector>, ps:Array<JDetector>, cur=null) {
        if (cur == null) cur = pane.graph.root;
        for (p in ps) {
            var box =
            switch (p.type) {
            case "BRISK":    createBrisk (pane, cast p);
            case "GFFT":     createGood  (pane, cast p);
            case "SURF":     createSurf  (pane, cast p);
            case "SIFT":     createSift  (pane, cast p);
            case "Masked":   createMasked(pane, cast p);
            case "Custom":   createCustom(pane, cast p);
            case x: throw 'Unknown detector type "$x"'; null;
            }
            cur.forth = box;
            cur = box;
        }
    }

    static function createBrisk(pane:ConfigPane<FeatureDetector>, ?init:JDBrisk=null) {
        var box = Box.createBox(pane.graph, 170, 100, "BRISK");
        var threshold = ConfigPane.genModifier("Threshold", 13*3);
        threshold.group.position([150-40,33]);
        threshold.inp.maxChars(4)
                     .allowed(~/[0-9]/)
                     .text(GLString.make(init == null ? "30" : Std.string(init.threshold), ColourScheme.base))
                     .commit();
        box.group.element(threshold.group);
        var octaves = ConfigPane.genModifier("Octaves", 13*3);
        octaves.group.position([150-40,33+18]);
        octaves.inp.maxChars(3)
                   .allowed(~/[0-9]/)
                   .text(GLString.make(init == null ? "3" : Std.string(init.octaves), ColourScheme.base))
                   .commit();
        box.group.element(octaves.group);
        var scale = ConfigPane.genModifier("Pattern Scale", 13*4);
        scale.group.position([150-40,33+18*2]);
        scale.inp.maxChars(5)
                 .allowed(~/[0-9.eE+\-]/)
                 .text(GLString.make(init == null ? "1.0" : Std.string(init.scale), ColourScheme.base))
                 .commit();
        box.group.element(scale.group);
        box.build = function () {
            var threshold = Std.parseInt(threshold.inp.getText().toString());
            var octaves   = Std.parseInt(octaves.inp.getText().toString());
            var scale     = Std.parseFloat(scale.inp.getText().toString());
            return new BriskDetector(cv.Features2D.brisk(threshold, octaves, scale));
        }
        box.write = function ():JDBrisk {
            return {
            type      : "BRISK",
            threshold : Std.parseInt(threshold.inp.getText().toString()),
            octaves   : Std.parseInt(octaves.inp.getText().toString()),
            scale     : Std.parseFloat(scale.inp.getText().toString())
            };
        }
        return box;
    }

    static function createGood(pane:ConfigPane<FeatureDetector>, ?init:JDGood=null) {
        var box = Box.createBox(pane.graph, 180, 150, "GFFT");
        var features = ConfigPane.genModifier("Max Features", 13*3);
        features.group.position([160-40,33]);
        features.inp.maxChars(4)
                    .allowed(~/[0-9]/)
                    .text(GLString.make(init == null ? "100" : Std.string(init.features), ColourScheme.base))
                    .commit();
        box.group.element(features.group);
        var quality = ConfigPane.genModifier("Quality", 13*4);
        quality.group.position([160-40,33+18]);
        quality.inp.maxChars(5)
                    .allowed(~/[0-9.eE+\-]/)
                    .text(GLString.make(init == null ? "0.1" : Std.string(init.quality), ColourScheme.base))
                    .commit();
        box.group.element(quality.group);
        var distance = ConfigPane.genModifier("Min Distance", 13*4);
        distance.group.position([160-40,33+18*2]);
        distance.inp.maxChars(5)
                    .allowed(~/[0-9.eE+\-]/)
                    .text(GLString.make(init == null ? "5.0" : Std.string(init.distance), ColourScheme.base))
                    .commit();
        box.group.element(distance.group);
        var block = ConfigPane.genModifier("Block Size", 13*3);
        block.group.position([160-40,33+18*3]);
        block.inp.maxChars(5)
                 .allowed(~/[0-9]/)
                 .text(GLString.make(init == null ? "3" : Std.string(init.block), ColourScheme.base))
                 .commit();
        box.group.element(block.group);
        var harris = ConfigPane.genCheckModifier("Harris Corners");
        harris.group.position([160-40,33+18*4]);
        harris.check.toggled(init == null ? false : init.harris);
        box.group.element(harris.group);
        var param = ConfigPane.genModifier("Harris Param.", 13*4);
        param.group.position([160-40,33+18*5]);
        param.inp.maxChars(5)
                 .allowed(~/[0-9.eE+\-]/)
                 .text(GLString.make(init == null ? "0.04" : Std.string(init.param), ColourScheme.base))
                 .commit();
        box.group.element(param.group);
        box.build = function () {
            var features = Std.parseInt(features.inp.getText().toString());
            var quality  = Std.parseFloat(quality.inp.getText().toString());
            var distance = Std.parseFloat(distance.inp.getText().toString());
            var block    = Std.parseInt(block.inp.getText().toString());
            var harris   = harris.check.getToggled();
            var param    = Std.parseFloat(param.inp.getText().toString());
            return new GoodFeaturesDetector(cv.Features2D.gfft(
                features, quality, distance, block,
                harris, param
            ));
        }
        box.write = function ():JDGood {
            return {
            type     : "GFFT",
            features : Std.parseInt(features.inp.getText().toString()),
            quality  : Std.parseFloat(quality.inp.getText().toString()),
            distance : Std.parseFloat(distance.inp.getText().toString()),
            block    : Std.parseInt(block.inp.getText().toString()),
            harris   : harris.check.getToggled(),
            param    : Std.parseFloat(param.inp.getText().toString())
            };
        }
        return box;
    }

    static function createSurf(pane:ConfigPane<FeatureDetector>, ?init:JDSurf=null) {
        var box = Box.createBox(pane.graph, 190, 110, "SURF");
        var hthreshold = ConfigPane.genModifier("Hessian T.hold", 13*4);
        hthreshold.group.position([170-40,33]);
        hthreshold.inp.maxChars(5)
                      .allowed(~/[0-9eE+\-]/)
                      .text(GLString.make(init == null ? "500" : Std.string(init.hthreshold), ColourScheme.base))
                      .commit();
        box.group.element(hthreshold.group);
        var octaves = ConfigPane.genModifier("Octaves", 13*2);
        octaves.group.position([170-40,33+18]);
        octaves.inp.maxChars(2)
                  .allowed(~/[0-9]/)
                  .text(GLString.make(init == null ? "4" : Std.string(init.octaves), ColourScheme.base))
                  .commit();
        box.group.element(octaves.group);
        var layers = ConfigPane.genModifier("Octave Layers", 13*2);
        layers.group.position([170-40,33+18*2]);
        layers.inp.maxChars(2)
                  .allowed(~/[0-9]/)
                  .text(GLString.make(init == null ? "2" : Std.string(init.layers), ColourScheme.base))
                  .commit();
        box.group.element(layers.group);
        var upright = ConfigPane.genCheckModifier("Upright");
        upright.group.position([170-40,33+18*3]);
        upright.check.toggled(init == null ? true : init.upright);
        box.group.element(upright.group);
        box.build = function () {
            var hthreshold = Std.parseFloat(hthreshold.inp.getText().toString());
            var octaves    = Std.parseInt(octaves.inp.getText().toString());
            var layers     = Std.parseInt(layers.inp.getText().toString());
            var upright    = upright.check.getToggled();
            return new SurfDetector(cv.NonFree.surf(
                hthreshold, octaves, layers, true, upright
            ));
        }
        box.write = function ():JDSurf {
            return {
            type       : "SURF",
            hthreshold : Std.parseFloat(hthreshold.inp.getText().toString()),
            octaves    : Std.parseInt(octaves.inp.getText().toString()),
            layers     : Std.parseInt(layers.inp.getText().toString()),
            upright    : upright.check.getToggled()
            };
        }
        return box;
    }

    static function createSift(pane:ConfigPane<FeatureDetector>, ?init:JDSift=null) {
        var box = Box.createBox(pane.graph, 190, 110, "SIFT");
        var features = ConfigPane.genModifier("Max Features", 13*3);
        features.group.position([170-40,33]);
        features.inp.maxChars(4)
                    .allowed(~/[0-9]/)
                    .text(GLString.make(init == null ? "100" : Std.string(init.features), ColourScheme.base))
                    .commit();
        box.group.element(features.group);
        var layers = ConfigPane.genModifier("Octave Layers", 13*2);
        layers.group.position([170-40,33+18]);
        layers.inp.maxChars(2)
                  .allowed(~/[0-9]/)
                  .text(GLString.make(init == null ? "3" : Std.string(init.layers), ColourScheme.base))
                  .commit();
        box.group.element(layers.group);
        var cthreshold = ConfigPane.genModifier("Constrast T.hold", 13*4);
        cthreshold.group.position([170-40,33+18*2]);
        cthreshold.inp.maxChars(5)
                      .allowed(~/[0-9eE+\-]/)
                      .text(GLString.make(init == null ? "0.04" : Std.string(init.cthreshold), ColourScheme.base))
                      .commit();
        box.group.element(cthreshold.group);
        var ethreshold = ConfigPane.genModifier("Edge T.hold", 13*4);
        ethreshold.group.position([170-40,33+18*3]);
        ethreshold.inp.maxChars(5)
                      .allowed(~/[0-9eE+\-]/)
                      .text(GLString.make(init == null ? "10.0" : Std.string(init.ethreshold), ColourScheme.base))
                      .commit();
        box.group.element(ethreshold.group);
        var sigma = ConfigPane.genModifier("σ", 13*4);
        sigma.group.position([170-40,33+18*2]);
        sigma.inp.maxChars(5)
                 .allowed(~/[0-9eE+\-]/)
                 .text(GLString.make(init == null ? "1.6" : Std.string(init.sigma), ColourScheme.base))
                 .commit();
        box.group.element(sigma.group);
        box.build = function () {
            var features   = Std.parseInt(features.inp.getText().toString());
            var layers     = Std.parseInt(layers.inp.getText().toString());
            var cthreshold = Std.parseFloat(cthreshold.inp.getText().toString());
            var ethreshold = Std.parseFloat(ethreshold.inp.getText().toString());
            var sigma      = Std.parseFloat(sigma.inp.getText().toString());
            return new SiftDetector(cv.NonFree.sift(
                features, layers, cthreshold, ethreshold, sigma
            ));
        }
        box.write = function ():JDSift {
            return {
            type       : "SIFT",
            features   : Std.parseInt(features.inp.getText().toString()),
            layers     : Std.parseInt(layers.inp.getText().toString()),
            cthreshold : Std.parseFloat(cthreshold.inp.getText().toString()),
            ethreshold : Std.parseFloat(ethreshold.inp.getText().toString()),
            sigma      : Std.parseFloat(sigma.inp.getText().toString())
            };
        }
        return box;
    }

    static function createMasked(pane:ConfigPane<FeatureDetector>, ?init:JDMasked=null) {
        var box = Box.createBox(pane.graph, 100, 65, "Masked", true);
        var display:TWindow = null;
        var button = null;
        box.data = [];
        if (init != null) {
            for (m in init.masks)
                box.data.push([for (p in m) new Vec2([p.x,p.y])]);
        }
        box.group.element(button = new PanelButton()
            .fit([15,33,70,23])
            .font(Fonts.dejavu)
            .text(GLString.make("Edit", ColourScheme.face))

            .press(function (_) {
                button.disabled(true);
                Main.window.main.sendMessage(TOpenWindow(Thread.current(), MaskedDisplay.displayWindow));
                GLFWEventLoop.waitEvent('DetectorPane', function (msg:TMessage) {
                    return switch (msg) {
                    case TOpened(displayWin):
                        display = displayWin;
                        true;
                    default: false;
                    };
                });
                Main.window.main.sendMessage(TContinue(Main.window));
                display.thread.sendMessage(TOther(SCallback(Main.window.thread)));
                display.thread.sendMessage(TOther(SData(null,box.data)));
                Main.addDisplay(display, function () {
                    button.disabled(false);
                    display = null;
                });
            })
            .commit()
        );
        box.build = function () {
            var masks:Array<Array<Vec2>> = box.data;
            var sub = box.side == null ? null : pane.graph.build(box.side.extract());
            if (sub == null) sub = new ConcatDetector();
            var det = new MaskedDetector(sub.extract());
            det.copy(masks);
            return det;
        }
        box.write = function ():JDMasked {
            var masks:Array<Array<Vec2>> = box.data;
            return {
            type    : "Masked",
            sub     : if (box.side == null) null
                      else pane.graph.write(box.side.extract()),
            masks   : masks.map(function (m)
                        return m.map(function (p:Vec2)
                            return {x:p.x,y:p.y}))
            };
        }
        if (init != null && init.sub != null) {
            parse(pane, init.sub, box);
            // parser shoves it into forth.
            box.side = box.forth;
            box.forth = null;
        }
        return box;
    }

    static function createCustom(pane:ConfigPane<FeatureDetector>, ?init:JDCustom=null) {
        var box = Box.createBox(pane.graph, 100, 65, "Custom");
        var display:TWindow = null;
        var button = null;
        box.data = (init == null) ? [] : init.features;
        box.group.element(button = new PanelButton()
            .fit([15,33,70,23])
            .font(Fonts.dejavu)
            .text(GLString.make("Edit", ColourScheme.face))

            .press(function (_) {
                button.disabled(true);
                Main.window.main.sendMessage(TOpenWindow(Thread.current(), CustomDisplay.displayWindow));
                GLFWEventLoop.waitEvent('DetectorPane', function (msg:TMessage) {
                    return switch (msg) {
                    case TOpened(displayWin):
                        display = displayWin;
                        true;
                    default: false;
                    };
                });
                Main.window.main.sendMessage(TContinue(Main.window));
                display.thread.sendMessage(TOther(SCallback(Main.window.thread)));
                display.thread.sendMessage(TOther(SData(null,box.data)));
                Main.addDisplay(display, function () {
                    button.disabled(false);
                    display = null;
                });
            })
            .commit()
        );
        box.build = function () {
            var gs:Array<{x:Float,y:Float,a:Null<Float>,r:Float}> = box.data;
            var features:Array<Feature> = [];
            for (g in gs) {
                var f = new Feature();
                f.set(g.x, g.y);
                var b = new BasicDescriptor();
                b.size = g.r*2;
                b.angle = if (g.a == null) -1 else 180+g.a*180/Math.PI;
                f.basics.push(b);
                features.push(f);
            }
            var count = Feature.merge(features, features.length);
            while (features.length > count) features.pop();
            return new CustomDetector(features);
        }
        box.write = function ():JDCustom {
            return {
            type     : "Custom",
            features : box.data
            };
        }
        return box;
    }
}


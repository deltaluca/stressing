package;

import cv.Features2D;
import cv.NonFree;

import hscript.Parser;
import hscript.Interp;

import ImageProcessor;
import FeatureDetector;
import FeatureDescriptor;

class Config {
    public static function loadCustom(path:String) {
        var interp = new Interp();
        var set = interp.variables.set;

        // OpenCV class access.
        set("cv", {
            "ImgProc":    cv.ImgProc,
            "Features2D": cv.Features2D,
            "NonFree":    cv.NonFree,
            "Core":       cv.Core,
            "HighGUI":    cv.HighGUI,
            "Video":      cv.Video,
            "core": {
                "Font":         cv.core.Font,
                "Image":        cv.core.Image,
                "Mat":          cv.core.Mat,
                "Point":        cv.core.Point,
                "Point2D32f":   cv.core.Point2D32f,
                "Point2D64f":   cv.core.Point2D64f,
                "Point3D32f":   cv.core.Point3D32f,
                "Point3D64f":   cv.core.Point3D64f,
                "Rect":         cv.core.Rect,
                "Scalar":       cv.core.Scalar,
                "Size":         cv.core.Size,
                "Size2D32f":    cv.core.Size2D32f,
                "TermCriteria": cv.core.TermCriteria
            },
            "features2d": {
                "BRISK":    cv.features2d.BRISK,
                "GFFT":     cv.features2d.GFFT,
                "KeyPoint": cv.features2d.KeyPoint,
            },
            "nonfree": {
                "SIFT": cv.nonfree.SIFT,
                "SURF": cv.nonfree.SURF
            },
            "highgui": {
                "Capture": cv.highgui.Capture
            }
        });

        var program = new Parser().parse(sys.io.File.read(path, false));
        return interp.execute(program);
    }
}
